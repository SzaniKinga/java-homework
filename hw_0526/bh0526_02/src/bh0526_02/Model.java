/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0526_02;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Model {

    private List<Employee> employees = new ArrayList<>();
    private int workerCount = 0;
    private int bossCount = 0;
    private static final String SER_FILENAME = "employees.ser";
    private static final String TEXT_FILENAME = "employees.txt";

    public String getCurrentTime() {
        LocalDateTime ldt = LocalDateTime.now();
        String formattedDate = ldt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return formattedDate;
    }

    public Worker createWorker() {
        Worker worker = new Worker("Worker" + workerCount++);
        registerEmployee(worker);
        return worker;
    }

    public Boss createBoss() {
        Boss boss = new Boss("Boss" + bossCount++);
        registerEmployee(boss);
        return boss;
    }

    private void registerEmployee(Employee employee) {
        employees.add(employee);
    }

    public void writeSerialized() {
        try (FileOutputStream fos = new FileOutputStream(SER_FILENAME);
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(employees);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void writeText() {
        File s = new File(SER_FILENAME);
        if (s.exists()) {
            List<Employee> employeeList;
            try (FileInputStream fis = new FileInputStream(SER_FILENAME);
                    ObjectInputStream ois = new ObjectInputStream(fis)) {
                employeeList = (ArrayList) ois.readObject();

                File f = new File(TEXT_FILENAME);
                try (FileWriter writer = new FileWriter(f)) {
                    for (Employee e : employeeList) {
                        writer.write(e.toString() + "\n");
                    }
                } catch (IOException e) {
                    System.out.println("A fájl nem található!");
                }
            } catch (IOException ex) {
                Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public String getSerializedFileName() {
        return this.SER_FILENAME;
    }

    public String getTextFileName() {
        return this.TEXT_FILENAME;
    }

    public String getEmployeeLog(Employee e) {
        StringBuilder sb = new StringBuilder();
        sb.append(getCurrentTime());
        sb.append(" ");
        sb.append(e.toString());
        sb.append("\n");
        return sb.toString();
    }

}
