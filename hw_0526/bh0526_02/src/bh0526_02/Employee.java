/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0526_02;

import java.io.Serializable;

public class Employee implements Serializable {
    
    private String name;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    

    @Override
    public String toString() {
        return "Employee{" + "name=" + name + '}';
    }
    
    
}
