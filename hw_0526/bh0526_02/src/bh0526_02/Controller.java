/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0526_02;

public class Controller {
    
    private MainFrame frame;
    private Model model;

    public Controller() {
        this.frame = new MainFrame("Swing", this);
        this.model = new Model();
        this.frame.init();
    }
    
    public void createWorker() {
        Worker worker = model.createWorker();
        frame.log(model.getEmployeeLog(worker));
    }

    public void createBoss() {
        Boss boss = model.createBoss();
        frame.log(model.getEmployeeLog(boss));
    }

    public void writeSerialized() {
        model.writeSerialized();
        frame.log("Employees have been serialized to " + model.getSerializedFileName()+"\n");
    }

    public void writeText() {
        model.writeText();
        frame.log("Employees have been written to " + model.getTextFileName()+"\n");
    }
    
}
