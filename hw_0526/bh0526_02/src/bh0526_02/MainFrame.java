/*Írj MVC-s SWING-es alkalmazást amin van egy darab textarea és négy darab gomb. 
Az első gombra kattintva egy Worker a másodikra egy Boss objektum jöjjön létre, 
úgy, hogy a textarea-ba logoljuk az eseményt (dátum-idő, milyen objektum jött létre). 
Mentsük le a Boss-t és a Workereket egy listába és tudjuk őket serializálni és 
beolvasni (3. és 4. gomb). Mentés és beolvasás mehet fix fájlba.
Hint: https://howtodoinjava.com/java/collections/arraylist/serialize-deserialize-arraylist/*/
package bh0526_02;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class MainFrame extends JFrame {

    private final JLabel label;
    private final JTextArea textArea;
    private final JButton workerButton;
    private final JButton bossButton;
    private final JButton saveButton;
    private final JButton serializeButton;
    private Controller controller;

    private static final String TEXT_FILE_NAME = "book.txt";
    private static final String SERIALIZED_FILE_NAME = "book.ser";
    

    public MainFrame(String title, Controller controller) {
        super(title);
        label = new JLabel("Book reader");
        textArea = new JTextArea(20, 30);
        workerButton = new JButton("Create worker");
        bossButton = new JButton("Create boss");
        saveButton = new JButton("Save");
        serializeButton = new JButton("Serialize");
        this.controller = controller;
    }

    public void init() {
        setSize(400, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.CENTER));
        add(label);
        add(textArea);
        add(workerButton);
        add(bossButton);
        add(saveButton);
        add(serializeButton);
        
        workerButton.addActionListener(event -> controller.createWorker());
        bossButton.addActionListener(event -> controller.createBoss());
        saveButton.addActionListener(event -> controller.writeText());
        serializeButton.addActionListener(event -> controller.writeSerialized());

        setVisible(true);
    }

    public void log(String log){
        textArea.append(log);
    }

    
}
