/*Írj programot (objektum orientáltan megtervezve), ami képes nyilvántartani műszaki 
cikkeket az IKEAM áruház számára. Minden műszaki cikknek pontosan egy típusa (olcsó, 
átlagos, drága, luxus, népszerű) van, továbbá van gyártója (String), vonalkódja, ára 
(int) és tudjuk a regisztrálás dátumát (ami a felvétel napja). A termékek a vonalkódok 
sorszáma alapján összehasonlíthatók. Ezzel a tulajdonsággal minden termék rendelkezik.
A műszaki cikkeknek 3 speciális típusa van, minden termék ebben a 3 csoport 
valamelyikében foglal helyet: konyhai eszközök, szórakoztató eszközök és 
szépségápolással kapcsolatos gépek. A konyhai eszközöket fel lehet emelni és be lehet 
kapcsolni. A szórakoztató eszközöket is be lehet kapcsolni, viszont 5 bekapcsolás után 
valamilyen hibát jeleznek (saját kivételosztály szükséges). A harmadik csoportnak 
van súlya kg-ban.
A raktárba érkezhetnek termékek és el is lehet őket adni. Az alábbi parancsokat a 
konzolon lehet megadni:
ADD vonalkód típus gyártó ár speciális_csoport → egy termék felvétele
REMOVE vonalkód → termék törlése
REPORT → report generálása

REPORT esetén az alábbi adatok jelennek meg a konzolon:
Hány termék lett kimentve?
Hány olyan szórakoztató eszköz van, ami még legalább 2 bekapcsolást bír?
(15 pont)*/
package bh0526_01;

import java.time.LocalDate;

public abstract class Product implements Comparable<Product> {
    
    private int barcode;
    private ProductType type;
    private String manufacturer;
    private int price;
    private LocalDate registrationDate;

    public Product(int barcode, ProductType type, String manufacturer, int price, LocalDate registrationDate) {
        this.barcode = barcode;
        this.type = type;
        this.manufacturer = manufacturer;
        this.price = price;
        this.registrationDate = registrationDate;
    }

    public int getBarcode() {
        return barcode;
    }

    public void setBarcode(int barcode) {
        this.barcode = barcode;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Override
    public String toString() {
        return "Product{" + "barcode=" + barcode + ", type=" + type + ", manufacturer=" + manufacturer + ", price=" + price + ", registrationDate=" + registrationDate + '}';
    }

}
