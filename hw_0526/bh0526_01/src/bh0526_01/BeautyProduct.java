
package bh0526_01;

import java.time.LocalDate;

public class BeautyProduct extends Product {
    
    private int weight;

    public BeautyProduct(int barcode, ProductType type, String manufacturer, int price, LocalDate registrationDate, int weight) {
        super(barcode, type, manufacturer, price, registrationDate);
        this.weight = weight;
    }


    @Override
    public int compareTo(Product o) {
       if (this.getBarcode() > o.getBarcode()) {
            return -1;
        } else if (this.getBarcode() == o.getBarcode()) {
            return 0;
        } else {
            return 1;
        }
    }

    
}
