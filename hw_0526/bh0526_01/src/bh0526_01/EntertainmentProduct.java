/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0526_01;

import java.time.LocalDate;

public class EntertainmentProduct extends Product implements Switchable {

    private int count = 0;

    public EntertainmentProduct(int barcode, ProductType type, String manufacturer, int price, LocalDate registrationDate) {
        super(barcode, type, manufacturer, price, registrationDate);
    }

    public int getCount() {
        return count;
    }

    @Override
    public int compareTo(Product o) {
        if (this.getBarcode() > o.getBarcode()) {
            return -1;
        } else if (this.getBarcode() == o.getBarcode()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public void turnOn() throws MyException {
        count++;
        if (count >= 5) {
            throw new MyException();
        } else {
            System.out.println("Bekapcsolások száma: " + count);
        }
    }

}
