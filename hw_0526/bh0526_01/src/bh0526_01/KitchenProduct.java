/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0526_01;

import java.time.LocalDate;

public class KitchenProduct extends Product implements Liftable, Switchable {

    public KitchenProduct(int barcode, ProductType type, String manufacturer, int price, LocalDate registrationDate) {
        super(barcode, type, manufacturer, price, registrationDate);
    }

    
    @Override
    public int compareTo(Product o) {
       if (this.getBarcode() > o.getBarcode()) {
            return -1;
        } else if (this.getBarcode() == o.getBarcode()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public void turnOn() {
        System.out.println("Bekapcsolva");
    }

    @Override
    public void lift() {
        System.out.println("Felemelve");
    }
    
    
}
