/*Írj programot (objektum orientáltan megtervezve), ami képes nyilvántartani műszaki 
cikkeket az IKEAM áruház számára. Minden műszaki cikknek pontosan egy típusa (olcsó, 
átlagos, drága, luxus, népszerű) van, továbbá van gyártója (String), vonalkódja, ára 
(int) és tudjuk a regisztrálás dátumát (ami a felvétel napja). A termékek a vonalkódok 
sorszáma alapján összehasonlíthatók. Ezzel a tulajdonsággal minden termék rendelkezik.
A műszaki cikkeknek 3 speciális típusa van, minden termék ebben a 3 csoport 
valamelyikében foglal helyet: konyhai eszközök, szórakoztató eszközök és 
szépségápolással kapcsolatos gépek. A konyhai eszközöket fel lehet emelni és be lehet 
kapcsolni. A szórakoztató eszközöket is be lehet kapcsolni, viszont 5 bekapcsolás után 
valamilyen hibát jeleznek (saját kivételosztály szükséges). A harmadik csoportnak 
van súlya kg-ban.
A raktárba érkezhetnek termékek és el is lehet őket adni. Az alábbi parancsokat a 
konzolon lehet megadni:
ADD vonalkód típus gyártó ár speciális_csoport → egy termék felvétele
REMOVE vonalkód → termék törlése
REPORT → report generálása

REPORT esetén az alábbi adatok jelennek meg a konzolon:
Hány termék lett kimentve?
Hány olyan szórakoztató eszköz van, ami még legalább 2 bekapcsolást bír?
(15 pont)*/
package bh0526_01;

import java.time.LocalDate;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {

    public static void main(String[] args) {

        Warehouse w1 = new Warehouse();
        String option;

        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Kérem, válasszon menüpontot:");
            System.out.println("ADD: egy termék felvétele");
            System.out.println("REMOVE: termék törlése");
            System.out.println("REPORT: report generálása");
            System.out.println("EXIT: kilépés");

            option = sc.nextLine();

            switch (option) {
                case "ADD":
                    System.out.println("Kérem, adja meg az adatokat: vonalkód, típus, gyártó, ár, speciális csoport");
                    String data = sc.nextLine();
                    String productData[] = data.split(" ");
                    int barcode = Integer.valueOf(productData[0]);
                    String type = productData[1];
                    String manufacturer = productData[2];
                    int price = Integer.valueOf(productData[3]);
                    String group = productData[4];
                    if (group.equals("BeautyProduct")) {
                        System.out.println("Kérem, adja meg a súlyát");
                        int weight = sc.nextInt();
                        BeautyProduct product = new BeautyProduct(barcode, ProductType.valueOf(type), manufacturer, price, LocalDate.now(), weight);
                        w1.add(product);
                        System.out.println(w1);
                    } else if (group.equals("EntertainmentProduct")) {
                        EntertainmentProduct product = new EntertainmentProduct(barcode, ProductType.valueOf(type), manufacturer, price, LocalDate.now());
                        int r = (int) (Math.random() * 5);
                        for (int i = 0; i < r; i++) {
                            try {
                                product.turnOn();
                            } catch (MyException ex) {
                                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        w1.add(product);

                        System.out.println(w1);
                    } else if (group.equals("KitchenProduct")) {
                        KitchenProduct product = new KitchenProduct(barcode, ProductType.valueOf(type), manufacturer, price, LocalDate.now());
                        int r = (int) (Math.random() * 5);
                        for (int i = 0; i < r; i++) {
                            product.turnOn();
                        }
                        if (Math.random() > 0.5) {
                            product.lift();
                        }
                        w1.add(product);
                        System.out.println(w1);
                    } else {
                        System.out.println("hibás adat!");
                    }
                    break;
                case "REMOVE":
                    System.out.println("Kérem, adja meg a törlendő termék vonalkódját");
                    int barcode1 = sc.nextInt();
                    w1.remove(barcode1);
                    break;
                case "REPORT":
                    System.out.println("Kimentett termékek száma: " + w1.getProductCount());
                    System.out.println("Még legalább 2 bekapcsolást bíró szórakoztató eszközök száma: " + w1.getOperationalEntertainmentProductCount());
                    break;
                default:
                    System.out.println("Érvénytelen parancs!");
            }
        } while (!option.equals("EXIT"));
        System.exit(0);

    }

}
