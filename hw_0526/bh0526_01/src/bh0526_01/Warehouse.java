/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0526_01;

import java.util.HashMap;
import java.util.Map;

public class Warehouse {

    private Map<Integer, Product> products = new HashMap<>();

    public void add(Product product) {
        products.put(product.getBarcode(), product);
    }

    public void remove(int barcode) {
        products.remove(barcode);
    }

    public int getProductCount() {
        return products.size();
    }


    public int getOperationalEntertainmentProductCount() {
        int count = 0;
        for (Product p : products.values()) {
            if (p instanceof EntertainmentProduct) {
                EntertainmentProduct ep = (EntertainmentProduct) p;
                if (ep.getCount() <= 2) {
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public String toString() {
        return "Warehouse{" + "products=" + products + '}';
    }

}
