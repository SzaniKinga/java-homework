/*2. Parancssorból kapott egész számok által alkotott intervallumból írjon 
ki egy véletlen számot a konzolra. */
package hw0326_06;

public class Hw0326_06 {

    public static void main(String[] args) {

        int[] numbersInt = convertNumber(args);
        System.out.println("A parancssorban megadott számok: ");
        printNumber(numbersInt);
        
        int minNumber = findMin(numbersInt);
        int maxNumber = findMax(numbersInt);
        int random = generateRandom(minNumber, maxNumber);
        System.out.println("Véletlen szám az adott intervallumban: " + random);
  
    } 
    
    public static int generateRandom(int min, int max) {
        int random = (int) (Math.random() * (max - min + 1) + min);
        return random;
    }

    public static int findMax(int[] array) {
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }
        return max;
    }

    public static int findMin(int[] array) {
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
        }
        return min;
    }

    public static void printNumber(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " "); 
        }
        System.out.println(" ");
    }

    public static int[] convertNumber(String[] array) {
        int[] numbers = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            numbers[i] = Integer.parseInt(array[i]);
        }
        return numbers;
    }

}
