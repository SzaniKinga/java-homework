//  1.    Parancssorból kapott egész számoknak írja ki a konzolra az összegüket.

package hw0326_05;

public class Hw0326_05 {

    public static void main(String[] args) {
        
        int[] numbersInt = convertNumber(args);
        System.out.println("A parancssorban megadott számok: ");
        printNumber(numbersInt);

        int sum = addNumbers(numbersInt);
        System.out.println("A számok összege: " + sum);
       
    }
    
    public static int addNumbers(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }
    
    public static void printNumber(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " "); 
        }
        System.out.println(" ");
    }
 
    public static int[] convertNumber(String[] array) {
        int[] numbers = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            numbers[i] = Integer.parseInt(array[i]);
        }
        return numbers;
    }

}
