/* 3.  Parancssorból kapott Stringeket vizsgáljuk meg, és írjuk ki azt, hogy 
  „Malacod van!” ha van a paraméterek között „pig” érték. */
package hw0326_07;

public class Hw0326_07 {

    public static void main(String[] args) {
        
        System.out.println("A parancssorban megadott Stringek: ");
        printString(args);
        
        boolean contains = containsPig(args);
        if (contains == true) {
            System.out.println("Malacod van!");
        }
        else {
            System.out.println("Nincs malacod!");
        }
    } 

    public static boolean containsPig(String[] array) {
        boolean contains = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals("pig") == true) {
                contains = true;
                break;
            } else {
                contains = false;
            }
        }
        return contains;
    }
    
     public static void printString(String[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " "); 
        }
        System.out.println(" ");
    }

}
