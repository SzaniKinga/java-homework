/*3.Hozzunk létre egy 2D tömböt a következőnek megfelelően: az első dimenzióban 
tároljuk el az alkalmazottak azonosítóját (egész számok, 1-től növekedve 1-gyel). 
A második dimenzióban tároljuk el az adott alkalmazotthoz tartozó havi fizetésüket.
A havi fizetés a következőképp alakul:
a.    Értékesítő kollégákról van szó, a fizetésük függ attól, hogy mennyi árut 
adnak el. Az alap bérük 450.000 Ft, ehhez adjunk hozzá véletlenszerűen 
50.000-125.000 Forintot.
b.    Minden negyedév végén kapnak véletlenszerűen az adott havi fizetésükhöz 
képest 5-15 százalékot.
Írasd ki a kollégák fizetését hónapra bontva.
Írjuk ki az éves szintű fizetést is. */
package hw0326_03;

public class Hw0326_03 {

    public static void main(String[] args) {

        int[][][] employeeData = new int[10][12][3];

        fillData(employeeData, 450000);
        printData(employeeData);
        printAnnualData(employeeData);

    }

    public static void printAnnualData(int[][][] array) {
        System.out.println("Éves szintű fizetésadatok: ");
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j][2];
            }
            System.out.println((i + 1) + ". személy éves fizetése: " + sum + " Ft");
        }
    }

    public static void printData(int[][][] array) {
        System.out.println("Fizetésadatok havi lebontásban: ");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j][1] + ". hó ");
                System.out.print("azon:" + array[i][j][0] + ". ");
                System.out.print(array[i][j][2] + " Ft ");
            }
            System.out.println(" ");
        }
    }

    public static void fillData(int[][][] array, int basicSalary) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[i][j].length; k++) {
                    array[i][j][0] = i + 1; //azonosító
                    array[i][j][1] = j + 1; //hónap
                    if ((i + 1) % 3 == 0) {
                        array[i][j][2] = calculateExtra(basicSalary + calculateBonus()); //fizetés
                    } else if ((i + 1) % 3 != 0) {
                        array[i][j][2] = basicSalary + calculateBonus();
                    }
                }
            }
        }
    }

    public static int calculateExtra(int salary) {
        return (int) (Math.random() * (salary * 1.15 - salary * 1.05 + 1) + salary * 1.05);
    }

    public static int calculateBonus() {
        return (int) (Math.random() * (125000 - 50000 + 1) + 50000);
    }

}
