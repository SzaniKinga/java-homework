/* 4. Az előző feladatot egészítsük ki a következőképpen.
Adott a következő két String halmaz:
a.    Laci, Józsi, Béla, Feri, Adri, Zsófi, Károly
b.    Nagy, Veréb, Molnár, Kiss, Papp, Tóth
Rendeljünk az előző feladatbeli azonosító számokhoz véletlenszerű neveket a 
fentebb felsorolt keresztnevek és vezetékneveket felhasználva, és így 
írassuk ki a kollégák fizetéseit. */
package hw0326_04;

import java.util.Random;

public class Hw0326_04 {

    public static void main(String[] args) {
        
        int[][][] employeeData = new int[10][12][3];
        
        String[] familyNames = {"Nagy", "Veréb", "Molnár", "Kiss", "Papp", "Tóth"};
        String[] firstNames = {"Laci", "Józsi", "Béla", "Feri", "Adri", "Zsófi", "Károly"};
        
        String[] names = generateNames(familyNames, firstNames);
        printNames(names);
        
        fillData(employeeData, 450000);
        printData(employeeData);
        printAnnualData(employeeData);

    }

    public static void printAnnualData(int[][][] array) {
        System.out.println("Éves szintű fizetésadatok: ");
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j][2];
            }
            System.out.println((i + 1) + ". személy éves fizetése: " + sum + " Ft");
        }
    }

    public static void printData(int[][][] array) {
        System.out.println("Fizetésadatok havi lebontásban: ");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j][1] + ". hó ");
                System.out.print("azon:" + array[i][j][0] + ". ");
                System.out.print(array[i][j][2] + " Ft ");
            }
            System.out.println(" ");
        }
    }

    public static void fillData(int[][][] array, int basicSalary) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[i][j].length; k++) {
                    array[i][j][0] = i + 1; //azonosító
                    array[i][j][1] = j + 1; //hónap
                    if ((i + 1) % 3 == 0) {
                        array[i][j][2] = calculateExtra(basicSalary + calculateBonus()); //fizetés
                    } else if ((i + 1) % 3 != 0) {
                        array[i][j][2] = basicSalary + calculateBonus();
                    }
                }
            }
        }
    }
    
    public static void printNames (String[] names) {
        System.out.println("A munkatársak nevei: ");
        for (int i = 0; i < names.length; i++) {
            System.out.println((i+1) + ".: " + names[i]);
        }
    }
    
    
    public static String[] generateNames (String[] familyNames, String[] firstNames) {
        String[] names = new String [10];
        for (int i = 0; i < names.length; i++) {
            Random rand = new Random();
        names[i] = familyNames[rand.nextInt(familyNames.length)] + " " + firstNames[rand.nextInt(firstNames.length)];
        }
        return names;
    }

    public static int calculateExtra(int salary) {
        return (int) (Math.random() * (salary * 1.15 - salary * 1.05 + 1) + salary * 1.05);
    }

    public static int calculateBonus() {
        return (int) (Math.random() * (125000 - 50000 + 1) + 50000);
    }

}
