insert into book(id,isbn,title,description) values('1','alma123','Pal utcai fiuk','Ez egy hosszu konyv');
insert into book(id,isbn,title,description) values('2','kalap321','Haboru es beke','Ez egy nagyon nagyon hosszu konyv');
insert into book(id,isbn,title,description) values('3','FDG222','Koszivu ember fiai','Ez egy szomoru konyv');

insert into author(id,name) values('1','jozsi');
insert into author(id,name) values('2','bela');
insert into author(id,name) values('3','sanyi');

insert into book_author_jt(book_id,author_id) values('1','1');
insert into book_author_jt(book_id,author_id) values('2','2');
insert into book_author_jt(book_id,author_id) values('3','3');