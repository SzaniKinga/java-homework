/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh12library.bean;

import hu.braininghub.bh12library.dto.BookDTO;
import java.util.List;

public class ListBooksPageBean { //servlet get metódusában kérjük majd ezt le, ennek a feladata a listBooks jsp kiszolgálása
    
    private List<BookDTO> books;

    public List<BookDTO> getBooks() {
        return books;
    }

    public void setBooks(List<BookDTO> books) {
        this.books = books;
    }
    
    
    
    
}
