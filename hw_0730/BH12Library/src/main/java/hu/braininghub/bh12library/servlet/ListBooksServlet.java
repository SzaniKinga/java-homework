/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh12library.servlet;

import hu.braininghub.bh12library.bean.ListBooksPageBean;
import hu.braininghub.bh12library.dto.BookFilterDTO;
import hu.braininghub.bh12library.entities.BookEntity;
import hu.braininghub.bh12library.mapper.BookMapper;
import hu.braininghub.bh12library.service.BookService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.mapstruct.factory.Mappers;

@WebServlet(name = "ListBooksServlet", urlPatterns = {"/listBooks"})
public class ListBooksServlet extends HttpServlet {
    
    private static final String LIST_BOOKS_PAGE_BEAN = "listBooksPageBean";
    
    @Inject
    private BookService bookService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        BookFilterDTO filterDTO = new BookFilterDTO();
        filterDTO.setIsbn(request.getParameter("isbn"));
        filterDTO.setTitle(request.getParameter("title"));
        filterDTO.setDescription(request.getParameter("description"));
        filterDTO.setAuthorName(request.getParameter("authorName"));
        
        List<BookEntity> books = bookService.findByFilter(filterDTO); //gomb megnyomásra a szűrt listát írja ki
        ListBooksPageBean listBooksPageBean = (ListBooksPageBean) request.getSession().getAttribute(LIST_BOOKS_PAGE_BEAN);
        
        if(listBooksPageBean == null) { //megnézzük, hogy a sessionben van-e listbookspagebean, ha nincs, akkor beállítjuk, ha van, akkor beállítom a könyvlistát
            listBooksPageBean = new ListBooksPageBean();
            request.getSession().setAttribute(LIST_BOOKS_PAGE_BEAN, listBooksPageBean);
        }
                
        listBooksPageBean.setBooks(BookMapper.INSTANCE.toDTOList(books));
        
        request.setAttribute("filterDTO", filterDTO);
        
        request.getRequestDispatcher("WEB-INF/listBooks.jsp").forward(request, response); //index.jspben ráklikkelnek a linkre, ide jutunk, listBookson pl keresőmezők lesznek
    } //gombot megnyomom -> indul egy post kérés

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        List<BookEntity> books = bookService.findAll(); //gomb megnyomásra megtörténik a findall()
        ListBooksPageBean listBooksPageBean = (ListBooksPageBean) request.getSession().getAttribute(LIST_BOOKS_PAGE_BEAN);
        
        if(listBooksPageBean == null) { //megnézzük, hogy a sessionben van-e listbookspagebean, ha nincs, akkor beállítjuk, ha van, akkor beállítom a könyvlistát
            listBooksPageBean = new ListBooksPageBean();
            request.getSession().setAttribute(LIST_BOOKS_PAGE_BEAN, listBooksPageBean);
        }
                
        listBooksPageBean.setBooks(BookMapper.INSTANCE.toDTOList(books));
        
        response.sendRedirect(request.getContextPath() + "/listBooks"); //redirect ugyanarra az oldalra
        
        
    }
    

}
