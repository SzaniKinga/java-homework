/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh12library.exception;

public class WrongBookTitleException extends RuntimeException {

    public WrongBookTitleException(String message) {
        super(message);
    }
    
    
    
}
