/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh12library.mapper;

import hu.braininghub.bh12library.dto.BookDTO;
import hu.braininghub.bh12library.entities.BookEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper
public interface BookMapper {
    
    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class); //ez alapból public static final
    
    BookDTO toDTO(BookEntity bookEntity);
    
    List<BookDTO> toDTOList(List<BookEntity> bookEntities);
    
    
}
