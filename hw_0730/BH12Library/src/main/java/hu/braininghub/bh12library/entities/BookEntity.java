/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh12library.entities;

import static hu.braininghub.bh12library.entities.BookEntity.QUERY_FIND_ALL;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "book")
@NamedQuery(name = QUERY_FIND_ALL, query = "select b from BookEntity b") //ez jpql, ami az entitásokon dolgozik... a name lesz a kulcs
public class BookEntity extends BaseEntity {

    public static final String QUERY_FIND_ALL = "BookEntity.findAll"; //dao-ból fogjuk ezt majd elérni

    @Column(unique = true)
    private String isbn;

    @Column
    private String title;

    //@Transient //ez egy olyan field, ami nem él a db-ben
    @Column
    private String description;

    //lazy típus: nem fog automatikusan egy joinos lekérdezést csinálni, gyorsabban betöltődik
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "book_author_jt",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    private List<AuthorEntity> authors = new ArrayList<>();

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AuthorEntity> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorEntity> authors) {
        this.authors = authors;
    }

    public void addAuthor(AuthorEntity entity) {
        authors.add(entity);
    }

}
