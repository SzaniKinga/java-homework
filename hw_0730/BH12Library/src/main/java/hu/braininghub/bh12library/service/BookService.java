/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh12library.service;

import hu.braininghub.bh12library.dao.BookDAO;
import hu.braininghub.bh12library.dto.BookFilterDTO;
import hu.braininghub.bh12library.entities.BookEntity;
import hu.braininghub.bh12library.exception.WrongBookTitleException;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

@Singleton
@TransactionAttribute
public class BookService {
    
    @Inject
    private BookDAO bookDAO;
    
    public List<BookEntity> findAll(){
        return bookDAO.findAll();
    }
    
    public List<BookEntity> findByFilter(BookFilterDTO filter){
        return bookDAO.findByFilter(filter);
    }
    
    
    public void save(BookEntity bookEntity){
        //ez egy tiltott könyv:
        if(bookEntity.getTitle().contains("utca")){
            throw new WrongBookTitleException("Hibás címet adtál meg a könyvhöz");
        }
        bookDAO.save(bookEntity);
    }
    
}
