/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh12library.dao;

import hu.braininghub.bh12library.dto.BookFilterDTO;
import hu.braininghub.bh12library.entities.BookEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Singleton
@TransactionAttribute
public class BookDAO {

    private static final String QUERY_FIND_BY_FILTER = "select b from BookEntity b ";

    @PersistenceContext
    private EntityManager em;

    public List<BookEntity> findAll() {
        return em.createNamedQuery(BookEntity.QUERY_FIND_ALL, BookEntity.class).getResultList(); //kulcsot meg kell adni és a class-t
    }

    public List<BookEntity> findByFilter(BookFilterDTO filter) {

        if ((filter.getAuthorName() == null || filter.getAuthorName().isEmpty())
                && (filter.getDescription() == null || filter.getDescription().isEmpty())
                && (filter.getIsbn() == null || filter.getIsbn().isEmpty())
                && (filter.getTitle() == null || filter.getTitle().isEmpty())) {
            return findAll();
        }

        String queryString = QUERY_FIND_BY_FILTER;
        if (!filter.getAuthorName().isEmpty()) {
            queryString += "join b.authors a where a.name like :name";
        }

        if (!filter.getTitle().isEmpty()) {
            if (queryString.contains("join")) {
                queryString += " and b.title like :title";
            } else {
                queryString += " where b.title like :title";
            }
        }

        if (!filter.getIsbn().isEmpty()) {
            if (queryString.contains("where")) {
                queryString += " and b.isbn like :isbn";
            } else {
                queryString += " where b.isbn like :isbn";
            }
        }

        if (!filter.getDescription().isEmpty()) {
            if (queryString.contains("where")) {
                queryString += " and b.description like :description";
            } else {
                queryString += " where b.description like :description";
            }
        }

        TypedQuery<BookEntity> query = em.createQuery(queryString, BookEntity.class);
        if (queryString.contains(":name")) {
            query.setParameter("name", "%" + filter.getAuthorName() + "%");
        }
        if (queryString.contains(":title")) {
            query.setParameter("title", "%" + filter.getTitle() + "%");
        }
        if (queryString.contains(":isbn")) {
            query.setParameter("isbn", "%" + filter.getIsbn() + "%");
        }
        if (queryString.contains(":description")) {
            query.setParameter("description", "%" + filter.getDescription() + "%");
        }

        List<BookEntity> resultList = query.getResultList();
        return resultList;

    }

    public void save(BookEntity bookEntity) {
        em.persist(bookEntity);
    }

}
