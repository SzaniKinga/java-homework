package hu.braininghub.bh12library.mapper;

import hu.braininghub.bh12library.dto.BookDTO;
import hu.braininghub.bh12library.entities.BookEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-07-30T20:36:25+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_251 (Oracle Corporation)"
)
public class BookMapperImpl implements BookMapper {

    @Override
    public BookDTO toDTO(BookEntity bookEntity) {
        if ( bookEntity == null ) {
            return null;
        }

        BookDTO bookDTO = new BookDTO();

        bookDTO.setId( bookEntity.getId() );
        bookDTO.setIsbn( bookEntity.getIsbn() );
        bookDTO.setTitle( bookEntity.getTitle() );
        bookDTO.setDescription( bookEntity.getDescription() );

        return bookDTO;
    }

    @Override
    public List<BookDTO> toDTOList(List<BookEntity> bookEntities) {
        if ( bookEntities == null ) {
            return null;
        }

        List<BookDTO> list = new ArrayList<BookDTO>( bookEntities.size() );
        for ( BookEntity bookEntity : bookEntities ) {
            list.add( toDTO( bookEntity ) );
        }

        return list;
    }
}
