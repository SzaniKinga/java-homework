/*Hozzunk létre az adatbázisunkban egy Site táblát (SiteId, Address).
A SiteServlet-en keresztül a felhasználó vehessen fel egy új telephelyet az adatbázisba.*/

package Servlets;

import Dao.SiteDao;
import Model.Site;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SiteServlet") 
public class SiteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        req.getRequestDispatcher("site.jsp").forward(req, res);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        Integer siteid = Integer.valueOf(req.getParameter("siteid"));
        String address = req.getParameter("address");

        Site s = new Site(siteid,address);
        SiteDao sd = new SiteDao();
        sd.save(s);
        
        res.sendRedirect("MainMenuServlet"); 
  
    }

}
