/*Hozzunk létre az adatbázisunkban egy Booking táblát (BookingId, StartDate, EndDate (LocalTime-ot használhatunk majd erre Java-ban)).
A BookingServlet-en keresztül a felhasználó vehessen fel egy új foglalást az adatbázisba.*/

package Servlets;

import Dao.BookingDao;
import Model.Booking;
import java.io.IOException;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/BookingServlet") 
public class BookingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        req.getRequestDispatcher("booking.jsp").forward(req, res);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        Integer bookingid = Integer.valueOf(req.getParameter("bookingid"));
        Integer carid = Integer.valueOf(req.getParameter("carid"));
        LocalDate startdate = LocalDate.parse(req.getParameter("startdate"));
        LocalDate enddate = LocalDate.parse(req.getParameter("enddate"));

        Booking b = new Booking(bookingid, carid, startdate, enddate);
        
        BookingDao bd = new BookingDao();
        bd.save(b);
        
        res.sendRedirect("MainMenuServlet"); 
  
    }

}
