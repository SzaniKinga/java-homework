/*Hozzunk létre az adatbázisunkban egy Car táblát (CarId, PlateNumber, Color, EngineType, Weight, YearOfBuilt, SiteId).
A CarServlet-en keresztül a felhasználó vehessen fel egy új autót az adatbázisba.*/
package Servlets;

import Dao.CarDao;
import Model.Car;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CarServlet")
public class CarServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        req.getRequestDispatcher("car.jsp").forward(req, res);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        Integer carid = Integer.valueOf(req.getParameter("carid"));
        String platenumber = req.getParameter("platenumber");
        String colour = req.getParameter("colour");
        String enginetype = req.getParameter("enginetype");
        Integer weight = Integer.valueOf(req.getParameter("weight"));
        Integer yearofbuilt = Integer.valueOf(req.getParameter("yearofbuilt"));
        Integer siteid = Integer.valueOf(req.getParameter("siteid"));
        
        Car c = new Car(carid, platenumber, colour, enginetype, weight, yearofbuilt, siteid);
        CarDao cd = new CarDao();
        cd.save(c);

        res.sendRedirect("MainMenuServlet");

    }

}
