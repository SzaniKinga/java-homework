/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.LocalDate;

public class Booking {
    
    private int bookingid;
    private int carid;
    private LocalDate startdate;
    private LocalDate enddate;

    public Booking(int bookingid, int carid, LocalDate startdate, LocalDate enddate) {
        this.bookingid = bookingid;
        this.carid = carid;
        this.startdate = startdate;
        this.enddate = enddate;
    }

    public int getBookingid() {
        return bookingid;
    }

    public void setBookingid(int bookingid) {
        this.bookingid = bookingid;
    }

    public int getCarid() {
        return carid;
    }

    public void setCarid(int carid) {
        this.carid = carid;
    }

    public LocalDate getStartdate() {
        return startdate;
    }

    public void setStartdate(LocalDate startdate) {
        this.startdate = startdate;
    }

    public LocalDate getEnddate() {
        return enddate;
    }

    public void setEnddate(LocalDate enddate) {
        this.enddate = enddate;
    }
    
    
    
}
