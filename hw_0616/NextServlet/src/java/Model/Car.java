/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

public class Car {

    private int carid;
    private String platenumber;
    private String colour;
    private String enginetype;
    private int weight;
    private int yearofbuilt;
    private int siteid;

    public Car(int carid, String platenumber, String colour, String enginetype, int weight, int yearofbuilt, int siteid) {
        this.carid = carid;
        this.platenumber = platenumber;
        this.colour = colour;
        this.enginetype = enginetype;
        this.weight = weight;
        this.yearofbuilt = yearofbuilt;
        this.siteid = siteid;
    }

    public int getCarid() {
        return carid;
    }

    public void setCarid(int carid) {
        this.carid = carid;
    }

    public String getPlatenumber() {
        return platenumber;
    }

    public void setPlatenumber(String platenumber) {
        this.platenumber = platenumber;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getEnginetype() {
        return enginetype;
    }

    public void setEnginetype(String enginetype) {
        this.enginetype = enginetype;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getYearofbuilt() {
        return yearofbuilt;
    }

    public void setYearofbuilt(int yearofbuilt) {
        this.yearofbuilt = yearofbuilt;
    }

    public int getSiteid() {
        return siteid;
    }

    public void setSiteid(int siteid) {
        this.siteid = siteid;
    }
    
    
    
    
}
