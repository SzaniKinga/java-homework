/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Site;
import Servlets.SiteServlet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SiteDao extends AbstractDao implements Dao<Site> {

    private static final String INSERT_QUERY = "INSERT INTO site (siteid, address) values(?, ?)";

    @Override
    public void save(Site s) {
        try (Connection conn = DriverManager.getConnection(getConnertionUrl(), "admin", "admin");
                PreparedStatement st = conn.prepareStatement(INSERT_QUERY);) {
            Class.forName("com.mysql.jdbc.Driver");

            st.setInt(1, s.getSiteid());
            st.setString(2, s.getAddress());

            st.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(SiteServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SiteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
