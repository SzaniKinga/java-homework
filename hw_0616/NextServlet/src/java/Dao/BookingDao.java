/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Booking;
import Servlets.BookingServlet;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BookingDao extends AbstractDao implements Dao<Booking>{
    
    private static final String INSERT_QUERY = "INSERT INTO booking (bookingid, carid, startdate, enddate) values(?, ?, ?, ?)";

    @Override
    public void save(Booking b) {
        try (Connection conn = DriverManager.getConnection(getConnertionUrl(), "admin", "admin");
                PreparedStatement st = conn.prepareStatement(INSERT_QUERY);) {
            Class.forName("com.mysql.jdbc.Driver"); 
            
            st.setInt(1, b.getBookingid());
            st.setInt(2, b.getCarid());
            st.setDate(3, Date.valueOf(b.getStartdate()));
            st.setDate(4, Date.valueOf(b.getEnddate()));
            
            int rows = st.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(BookingServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BookingServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
