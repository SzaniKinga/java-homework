/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Car;
import Servlets.CarServlet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CarDao extends AbstractDao implements Dao<Car>{
    
    private static final String INSERT_QUERY = "INSERT INTO car (carid, platenumber, colour, enginetype, weight, yearofbuilt, siteid) values(?, ?, ?, ?, ?, ?, ?)";

    @Override
    public void save(Car c) {
        try (Connection conn = DriverManager.getConnection(getConnertionUrl(), "admin", "admin");
                PreparedStatement st = conn.prepareStatement(INSERT_QUERY);) {
            Class.forName("com.mysql.jdbc.Driver"); 
            
            st.setInt(1, c.getCarid());
            st.setString(2, c.getPlatenumber());
            st.setString(3, c.getColour());
            st.setString(4, c.getEnginetype());
            st.setInt(5, c.getWeight());
            st.setInt(6, c.getYearofbuilt());
            st.setInt(7, c.getSiteid());
            
            int rows = st.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(CarServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CarServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
