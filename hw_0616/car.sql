CREATE DATABASE IF NOT EXISTS bh12_carLibrary;

USE bh12_carLibrary;

DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS car;
DROP TABLE IF EXISTS site;

CREATE TABLE IF NOT EXISTS site (
	siteid INT AUTO_INCREMENT PRIMARY KEY,
    address VARCHAR(127)
);

CREATE TABLE IF NOT EXISTS car (
	carid INT AUTO_INCREMENT PRIMARY KEY,
    siteid INT NOT NULL,
    platenumber VARCHAR(127),
    colour VARCHAR(127),
    enginetype VARCHAR(127),
    weight INT,
    yearofbuilt INT,
    FOREIGN KEY (siteid) REFERENCES site(siteid)
);

CREATE TABLE IF NOT EXISTS booking (
	bookingid INT AUTO_INCREMENT PRIMARY KEY,
    carid INT NOT NULL,
    startdate DATE,
    enddate DATE,
    FOREIGN KEY (carid) REFERENCES car(carid)
);
