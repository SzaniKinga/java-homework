/*Legyenek Orszagaink, abban vannak Varosok. Varosoknak van nepesseguk es nevuk. 
Akkor egyenlo ket orszag, ha ugyan azok a varosok vannak bennuk. Es akkor egyenlo 
ket varos, ha a nevuk es a nepesseguk megegyezik egymassal. Csinaljatok teszt 
orszagokat, duplikaciokkal egyutt, es rakosgassatok oket be valamilyen Listbe. 
Toroljetek a listabol egy adott orszagot. Valamint ellenorizzetek, hogy ugyan 
olyan orszag nem kerul be duplikalva egy HashSetbe. Rakjatok bele ezeket 
orszagokat egy TreeSetbe is az orszag ossznepessege alapjan ami a varosok 
nepessegenek az osszege. */
package hw0418;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        Country hungary = new Country("Hungary");
        hungary.addCity(new City("Budapest", 5));
        hungary.addCity(new City("Pécs", 3));
        hungary.addCity(new City("Szeged", 2));
        
        Country germany = new Country("Germany");
        germany.addCity(new City("Munchen", 7));
        germany.addCity(new City("Koln", 6));
        germany.addCity(new City("Aachen", 7));
        
        Country hungary2 = new Country("New Hungary");
        hungary2.addCity(new City("Budapest", 5));
        hungary2.addCity(new City("Szeged", 2));
        hungary2.addCity(new City("Pécs", 3));
        hungary2.addCity(new City("Érd", 3));
        
        System.out.println(hungary.equals(hungary2));
        
        List<Country> countries = new ArrayList<>();
        countries.add(hungary);
        countries.add(germany);
        countries.add(hungary2);
        countries.add(germany);
        
        for(Country country: countries) {
            System.out.println(country);
        }
        
        countries.remove(germany);
        System.out.println("---------------------");
         for(Country country: countries) {
            System.out.println(country);
        }
         
        Set<Country> countries2 = new HashSet<>();
        countries2.add(hungary2);
        countries2.add(hungary);
        countries2.add(germany);
        countries2.add(hungary);
        
        System.out.println("---------------------");
        for(Country country: countries2) {
            System.out.println(country);
        }
        
        Set<Country> countries3 = new TreeSet<>();
        countries3.add(hungary2);
        countries3.add(germany);
        countries3.add(hungary);
        countries3.add(germany);
        
        System.out.println("---------------------");
        for(Country country: countries3) {
            System.out.println(country);
        }
        
    }
    
}
