/*Legyenek Orszagaink, abban vannak Varosok. Varosoknak van nepesseguk es nevuk. 
Akkor egyenlo ket orszag, ha ugyan azok a varosok vannak bennuk. Es akkor egyenlo 
ket varos, ha a nevuk es a nepesseguk megegyezik egymassal. Csinaljatok teszt 
orszagokat, duplikaciokkal egyutt, es rakosgassatok oket be valamilyen Listbe. 
Toroljetek a listabol egy adott orszagot. Valamint ellenorizzetek, hogy ugyan 
olyan orszag nem kerul be duplikalva egy HashSetbe. Rakjatok bele ezeket 
orszagokat egy TreeSetbe is az orszag ossznepessege alapjan ami a varosok 
nepessegenek az osszege. */
package hw0418;

import java.util.Objects;

public class City {
    private String name;
    private int population;

    public City(String name, int population) {
        this.name = name;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + this.population;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final City other = (City) obj;
        if (this.population != other.population) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "City{" + "name=" + name + ", population=" + population + '}';
    }
    
    
}
