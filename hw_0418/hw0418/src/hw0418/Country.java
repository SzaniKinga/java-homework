/*Legyenek Orszagaink, abban vannak Varosok. Varosoknak van nepesseguk es nevuk. 
Akkor egyenlo ket orszag, ha ugyan azok a varosok vannak bennuk. Es akkor egyenlo 
ket varos, ha a nevuk es a nepesseguk megegyezik egymassal. Csinaljatok teszt 
orszagokat, duplikaciokkal egyutt, es rakosgassatok oket be valamilyen Listbe. 
Toroljetek a listabol egy adott orszagot. Valamint ellenorizzetek, hogy ugyan 
olyan orszag nem kerul be duplikalva egy HashSetbe. Rakjatok bele ezeket 
orszagokat egy TreeSetbe is az orszag ossznepessege alapjan ami a varosok 
nepessegenek az osszege. */
package hw0418;

import java.util.ArrayList;
import java.util.List;

public class Country implements Comparable<Country> {

    private String name;

    public Country(String name) {
        this.name = name;
    }

    List<City> cities = new ArrayList<>();

    public void addCity(City city) {
        cities.add(city);
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Country other = (Country) obj;
        if (cities.size() != other.cities.size()) {
            return false;
        }
        for (City city : cities) {
            boolean found = false;
            for (City otherCity : other.cities) {
                if (city.equals(otherCity)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "Country{" + "name=" + name + ", cities=" + cities + '}';
    }

    @Override
    public int compareTo(Country o) {
        if (getPopulation() == o.getPopulation()) {
            return 0;
        } else if (getPopulation() > o.getPopulation()) {
            return 1;
        } else {
            return -1;
        }

    }

    public int getPopulation() {
        int population = 0;
        for (City city : cities) {
            population += city.getPopulation();
        }
        return population;
    }

}
