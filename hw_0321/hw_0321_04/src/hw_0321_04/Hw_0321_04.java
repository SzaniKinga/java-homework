/*4. A program legyen képes ötös és hatos lottó egy lehetséges húzásának 
eredményét visszaadni. A program köszöntse a felhasználót, majd egy menüvel 
döntse el, hogy ötös vagy hatos lottó számokat adjon vissza (menü) */
package hw_0321_04;

import java.util.Scanner;

public class Hw_0321_04 {

    public static final int EXIT_MENU_NUMBER = 4;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number;
        do {
            System.out.println("Kérem, adjon meg egy számot:");
            System.out.println("1. Köszöntés");
            System.out.println("2. Az ötöslottó számai");
            System.out.println("3. A hatoslottó számai");
            System.out.println(EXIT_MENU_NUMBER + ". Kilépés a menüből");
            number = sc.nextInt();
            switch (number) {
                case 1:
                    sayHello();
                    break;
                case 2:
                    lotteryFive();
                    break;
                case 3:
                    lotterySix();
                    break;
            }
        } while (number != EXIT_MENU_NUMBER);

    }

    public static void sayHello() {
        System.out.println("Jó napot kívánok!");
    }

    public static void lotteryFive() {
        int[] arr5 = new int[5];
        for (int i = 0; i < 5; i++) {
            arr5[i] = (int) (Math.random() * (90 - 1 + 1) + 1);
        }
        System.out.println("Az ötöslottó számai: ");
        for (int i = 0; i < 5; i++) {
            System.out.print(arr5[i] + ", ");
        }
        System.out.println(" ");
    }

    public static void lotterySix() {
        int[] arr5 = new int[6];
        for (int i = 0; i < 6; i++) {
            arr5[i] = (int) (Math.random() * (45 - 1 + 1) + 1);
        }
        System.out.println("A hatoslottó számai: ");
        for (int i = 0; i < 6; i++) {
            System.out.print(arr5[i] + ", ");
        }
        System.out.println(" ");
    }

}
