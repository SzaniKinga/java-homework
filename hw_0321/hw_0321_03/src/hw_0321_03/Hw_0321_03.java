/*3. A program állítson elő véletlenszerűen két megegyező méretű mátrixot! 
Számítsa ki a és írja ki a két mátrix összegét! Minden részfeladatot külön 
metódus valósítson meg! */

package hw_0321_03;

public class Hw_0321_03 {

    public static void main(String[] args) {
        
        getSumArray(6, 6, 1, 9);

    }
    
    public static void getSumArray(int size1, int size2, int min, int max) {
        int array1[][] = new int[size1][size2];
        int array2[][] = new int[size1][size2];
        for (int i = 0; i < size1; i++) {
            for (int j = 0; j < size2; j++) {
        array1[i][j] = (int)(Math.random()*(max-min+1)+min);
        array2[i][j] = (int)(Math.random()*(max-min+1)+min);
        }
        }
         System.out.println("Az első random mátrix:"); 
        for (int i = 0; i < size1; i++) { 
            for (int j = 0; j < size2; j++) {
            System.out.print(array1[i][j] + " ");
           }
         System.out.println(" ");    
        }
        System.out.println("A második random mátrix:"); 
        for (int i = 0; i < size1; i++) { 
            for (int j = 0; j < size2; j++) {
            System.out.print(array2[i][j] + " ");
           }
         System.out.println(" ");    
        }
        int arraySum[][] = new int[size1][size2];
        System.out.println("Az összeg mátrix:"); 
        for (int i = 0; i < size1; i++) { 
            for (int j = 0; j < size2; j++) {
        arraySum[i][j] = array1[i][j] + array2[i][j];
        System.out.print(arraySum[i][j] + " ");
        }
         System.out.println(" ");    
        }
        
    }
    
  
    public static void printMultiArray (int size1, int size2, int min, int max) {
        int array1[][] = new int[size1][size2];
        int array2[][] = new int[size1][size2];
        for (int i = 0; i < size1; i++) {
            for (int j = 0; j < size2; j++) {
        array1[i][j] = (int)(Math.random()*(max-min+1)+min);
        array2[i][j] = (int)(Math.random()*(max-min+1)+min);
        }
        }
        System.out.println("Az első random mátrix:"); 
        for (int i = 0; i < size1; i++) { 
            for (int j = 0; j < size2; j++) {
            System.out.print(array1[i][j] + " ");
           }
         System.out.println(" ");    
        }
        System.out.println("A második random mátrix:"); 
        for (int i = 0; i < size1; i++) { 
            for (int j = 0; j < size2; j++) {
            System.out.print(array2[i][j] + " ");
           }
         System.out.println(" ");    
        }
        }
    }  

