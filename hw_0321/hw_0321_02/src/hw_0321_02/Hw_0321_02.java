/*2. A program töltsön fel véletlenszerűen kétjegyű számokkal egy 500 elemű tömböt, 
majd csináljon statisztikát róla, melyik szám hányszor fordul elő! */
package hw_0321_02;

public class Hw_0321_02 {

    public static void main(String[] args) {

        int[] array = fillRandomArray(500, 10, 99);

        getItems(array);

        System.out.println(" ");

        countItems(array);

    }

    public static void countItems(int[] arr) {
        int[] counter = new int[100];
        for (int i = 10; i < arr.length; i++) {
            counter[arr[i]]++;
        }
        System.out.println("A tömbben lévő számok darabszáma: ");
        for (int i = 10; i < counter.length; i++) {
            System.out.println(i + ": " + counter[i] + " db");
        }

    }

    public static void getItems(int[] arr) {
        System.out.println("A random tömb elemei: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + ", ");
        }

    }

    public static int[] fillRandomArray(int size, int min, int max) {
        int[] arr = new int[size];

        for (int i = 0; i < size; i++) {
            arr[i] = (int) (Math.random() * (max - min + 1) + min);

        }
        return arr;

    }

}
