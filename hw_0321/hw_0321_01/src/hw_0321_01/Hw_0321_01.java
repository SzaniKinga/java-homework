/*1. A program dolgozzon egy 20 elemű tömbbel, töltse fel véletlen kétegyű számokkal! 
Minden részfeladatot külön metódusban kell megvalósítani
    - Listázza a tömbelemeket
    - Páros tömbelemek összege
    - Van-e öttel osztható szám
    - Melyik az első páratlan szám a tömbben
    - Van-e a tömbben 32 */
package hw_0321_01;

public class Hw_0321_01 {

    public static void main(String[] args) {

        int[] array = fillRandomArray(20, 10, 99);
        
        getItems(array);

        System.out.println("");

        System.out.println("A páros tömbelemek összege: " + getSumEven(array));

        isDividableByFive(array);

        System.out.println("Az első páratlan elem: " + getFirstOdd(array));

        containsThirtytwo(array);
    }

    public static void containsThirtytwo(int[] arr) {
        int containsThirtytwo = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 32) {
                containsThirtytwo++;
            }
        }
        if (containsThirtytwo == 0) {
            System.out.println("Nincs a tömbben 32");
        } else {
            System.out.println("Van a tömbben 32");
        }
    }

    public static int getFirstOdd(int[] arr) {
        int oddNumber = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 1) {
                oddNumber = arr[i];
                break;
            }
        }
        return oddNumber;
    }

    public static void isDividableByFive(int[] arr) {
        int isDividable = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 5 == 0) {
                isDividable++;
            }
        }
        if (isDividable == 0) {
            System.out.println("Nincs öttel osztható elem");
        } else {
            System.out.println("Van öttel osztható elem");
        }
    }

    public static int getSumEven(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                sum += arr[i];
            }
        }
        return sum;
    }

    public static void getItems(int[] arr) {
        System.out.println("A random tömb elemei: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + ", ");
        }

    }

    public static int[] fillRandomArray(int size, int min, int max) {
        int[] arr = new int[size];

        for (int i = 0; i < size; i++) {
            arr[i] = (int) (Math.random() * (max - min + 1) + min);

        }
        return arr;

    }

}
