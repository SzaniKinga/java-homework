/* 5. fibonacci sorozat n. elemének kiszámolására, oldja meg hagyományos 
és rekurzív módon */
package hw_0321_05;

public class Hw_0321_05 {

    public static void main(String[] args) {

        fibonacci(7);
        System.out.println(" ");
        System.out.println(fibonacciElement(7));
        int number = 7;
        System.out.print("A Fibonacci sorozat " + number + ". eleme (rekurzív): ");
        System.out.println(recFibonacci(7));
    }

    public static void fibonacci(int number) {
        int t1 = 1;
        int t2 = 1;
        int i = 1;
        System.out.print("A Fibonacci sorozat a(z) " + number + ". elemig: ");
        System.out.println(" ");
        while (i <= number) {
            System.out.print(t1 + ", ");
            int sum = t1 + t2;
            t1 = t2;
            t2 = sum;
            i++;
        }
    }

    public static int fibonacciElement(int number) {
        System.out.print("A Fibonacci sorozat " + number + ". eleme: ");
        if (number == 0) { 
            return 0; 
        } 
        if (number == 1){ 
            return 1; 
        } 
        int t1 = 1;
        int t2 = 1;
        int nth = 1;
        for (int i = 3; i <= number; i++) { 
            nth = t1 + t2; 
            t1 = t2; 
            t2 = nth; 
        } 
        return nth; 
    }
   
   
    public static int recFibonacci(int number) {
        if (number <= 1) {
            return number;
        }
        return recFibonacci(number - 1) + recFibonacci(number - 2);
    }

}
