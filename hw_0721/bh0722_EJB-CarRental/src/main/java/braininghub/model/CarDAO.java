package braininghub.model;

import braininghub.entities.Car;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

@Singleton
public class CarDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    public void addNewCar(CarDTO dto) {
        Car c = CarMapper.toEntity(dto);
        em.persist(c); //egy elem eltárolása
    }
    
    public void removeCar(int id) {
        Car c = this.getCarById(id);
        em.remove(c);
    }

    public Car getCarById(int id) {
        Car c = em.find(Car.class, id); //egy elem megtalálása
        return c;
    }

    public List<CarDTO> getCars() { //a listát akarom lekérni adatbázisból
        List<Car> cars = em.createQuery("SELECT c FROM Car c").getResultList(); //ez a JPQL, utolsó m egy alias név, getResultList olyan mint az executeQuery
        List<CarDTO> carDTOs = new ArrayList();
        
        for(Car car: cars){
            carDTOs.add(CarMapper.toDTO(car)); //itt átmappeljük DTO-ra
        }
        
        return carDTOs;
    }

    
    
}
