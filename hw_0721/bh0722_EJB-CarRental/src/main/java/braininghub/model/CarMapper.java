/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package braininghub.model;

import braininghub.entities.Car;

public class CarMapper {

    public static CarDTO toDTO(Car car) {
        CarDTO dto = new CarDTO();
        
        dto.setId(car.getId());
        dto.setDetails(car.getDetails());

        return dto;
    }
    
    public static Car toEntity(CarDTO dto) {
        Car car = new Car();
        
        car.setId(dto.getId());
        car.setDetails(dto.getDetails());

        return car;
    }


}
