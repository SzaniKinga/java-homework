/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package braininghub.service;

import braininghub.entities.Car;
import braininghub.model.CarDAO;
import braininghub.model.CarDTO;
import java.sql.SQLException;
import java.util.List;
import javax.inject.Inject;


public class CarService {

    @Inject 
    CarDAO dao; 
    
    public void addCar(String details) {
        CarDTO dto = new CarDTO();
        dto.setDetails(details);
        
        dao.addNewCar(dto);
    }

    public void removeCar(int messageId) {
        dao.removeCar(messageId);
    }

    public List<CarDTO> getCars() throws SQLException {
        return dao.getCars();
    }

    public Car getCarById(int id) {
        return dao.getCarById(id);
    }
}
