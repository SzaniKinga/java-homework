insert into company(id, name) values (-10, 'Company1');
insert into company(id, name) values (-11, 'Company2');

insert into site(id, address, company_id) values (-10, 'Budapest Kálvin tér 2', -10);
insert into site(id, address, company_id) values (-11, 'Miskolc Maros tér 122', -11);
insert into site(id, address, company_id) values (-12, 'Pécs Kossuth utca 4', -10);

insert into car(carid, color, site_id) values (-10, 'fekete', -10);
insert into car(carid, color, site_id) values (-11, 'barna', -10);
insert into car(carid, color, site_id) values (-12, 'fehér', -11);

