/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh0721_jpamain;

import hu.braininghub.bh0721_jpamain.entities.Car;
import hu.braininghub.bh0721_jpamain.entities.Company;
import hu.braininghub.bh0721_jpamain.entities.Site;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpamain"); //persistence unit nevét kell itt megadni a zárójelben
        EntityManager em = factory.createEntityManager(); //itt hozzuk létre az entitymanagert, itt nincs jdbc pool, csak sima jdbc
        
        /*List<Car> carList = em.createQuery("select c from Car c").getResultList(); //adatbázis lekérdezést lefuttatjuk, listába tesszük

        for (Car c : carList) {
            System.out.println(c);
            System.out.println(c.getSite().getAddress()); //itt olyan lesz, mint a join, hozzáírjuk az addresst
        }*/
        
        /*List<Site> siteList = em.createQuery("select s from Site s").getResultList(); //adatbázis lekérdezést lefuttatjuk, listába tesszük

        for (Site s : siteList) {
            System.out.println(s);
            System.out.println("Car color=" + s.getCar().getColor());
        }*/
        
        /*List<Car> carList = em.createQuery("select c from Car c").getResultList(); //adatbázis lekérdezést lefuttatjuk, listába tesszük

        for (Car c : carList) {
            System.out.println(c);
            System.out.println("Site address=" + c.getSite().getAddress());
            System.out.println("Company name=" + c.getSite().getCompany().getName()); 
        }*/
                
        List<Company> companyList = em.createQuery("select cp from Company cp").getResultList();
        
        for (Company cp : companyList) {
            System.out.println(cp); 
        }

        
    }

}
