/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh0721_jpamain.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Site implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO) 
    private Long id;
    
    @Column
    private String address;
    
    @OneToMany(mappedBy = "site", fetch=FetchType.EAGER) //ezzel jelzem, hogy a car táblában lesz a foreign key, nem a siteban! "site" aminek a Car entityben neveztem
    private List<Car> cars;
    
    @ManyToOne 
    @JoinColumn(name = "company_id") //ez foreign key lesz
    private Company company; 

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Site{" + "id=" + id + ", address=" + address + ", cars=" + cars + '}';
    }

    
}
