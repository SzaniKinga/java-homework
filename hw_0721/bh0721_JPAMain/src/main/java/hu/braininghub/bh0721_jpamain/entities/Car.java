/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh0721_jpamain.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Car implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO) 
    private int carId;
    
    @Column
    private String color;
    
    @ManyToOne //ezzel jelzem, hogy ez egy foreign key lesz
    @JoinColumn(name = "site_id") //ezzel jelzem, hogy mi a foreign key neve
    private Site site; //több-1 kapcsolat
    

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }
    
    @Override
    public String toString() {
        return "Car{" + "carId=" + carId + ", color=" + color + '}';
    }

   
    
}
