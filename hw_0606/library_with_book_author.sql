/* show databases;  megmutatja az elerheto db-ket */

CREATE DATABASE IF NOT EXISTS bh12_library;

USE bh12_library;

DROP TABLE IF EXISTS books_author;
DROP TABLE IF EXISTS rent;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS author;
DROP TABLE IF EXISTS visitor;

CREATE TABLE IF NOT EXISTS author (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(256),
    birth_year INT,
    country VARCHAR(128)
);

CREATE TABLE IF NOT EXISTS visitor (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(256),
    birth_date DATE,
    address VARCHAR(512),
    phone VARCHAR(64)
);

CREATE TABLE IF NOT EXISTS categories (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(127)
);

CREATE TABLE IF NOT EXISTS books (
	id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(512) NOT NULL,
    year_of_issue INT,
    description TEXT,
    category_id INT,
    rentable BIT,
    max_rent_time VARCHAR(128),
    FOREIGN KEY (category_id) REFERENCES categories(id)
);

CREATE TABLE IF NOT EXISTS books_author (
	books_id INT NOT NULL,
    author_id INT NOT NULL,
    FOREIGN KEY (books_id) REFERENCES books(id),
    FOREIGN KEY (author_id) REFERENCES author(id)
);

ALTER TABLE books_author ADD PRIMARY KEY (books_id, author_id);

CREATE TABLE IF NOT EXISTS rent (
	id INT AUTO_INCREMENT PRIMARY KEY,
    visitor_id INT,
    book_id INT,
    rent_date DATE,
    FOREIGN KEY (book_id) REFERENCES books(id),
    FOREIGN KEY (visitor_id) REFERENCES visitor(id)
);

/*SHOW TABLES;*/

/*DESCRIBE books;*/

INSERT INTO author (name, birth_year, country) VALUES ('Jonas Jonasson', 1940, 'Swedish');
INSERT INTO author (name, birth_year, country) VALUES ('Robert C. Martin',1938, 'English');
INSERT INTO author (name, birth_year, country) VALUES ('X', 1942,'English');
INSERT INTO author (name, birth_year, country) VALUES ('Y', 1955, null);
INSERT INTO author (name, birth_year, country) VALUES ('Z', 2002, null);

INSERT INTO categories (name) VALUES('vigjatek');
INSERT INTO categories (name) VALUES('szakmai');
INSERT INTO categories (name) VALUES('A');
INSERT INTO categories (name) VALUES('B');
INSERT INTO categories (name) VALUES('C');

INSERT INTO books (title, year_of_issue, description, category_id, rentable, max_rent_time) 
		    VALUES 
				('szaz eves ember', 2009, 'kiraly konyv', 1, 1, '2 het'),
				('tiszta kod', 2007, 'biblie', 2, 1, '2 het'),
                ('tisztatalan kod', 2007, 'anti biblie', 2, 1, '2 het'),
                ('B', 2007, 'anti biblie', 2, 1, '2 het'),
                ('C', 2020, 'anti biblie', 2, 1, '2 het'),
                ('D', 2009, 'anti biblie', 3, 1, '1 het'),
                ('E', 2002, 'anti biblie', 2, 1, '2 het'),
                ('F', 2001, 'anti biblie', 4, 1, '2 het'),
                ('G', 1998, 'anti biblie', 5, 1, '2 het'),
                ('H', 2000, 'anti biblie', 2, 1, '2 het'),
                ('I', 1780, 'anti biblie', 3, 0, null),
                ('J', 2010, 'anti biblie', 2, 1, '2 het');
                
INSERT INTO books_author (books_id, author_id) VALUES 
				(1,1), (2,2), (4,2), (5,3), (6,3), (6,2), (7,2), (7,3), 
                (8,2), (9,2), (10,1), (10,2), (11,1), (12,4);

INSERT INTO visitor (name, birth_date) VALUES ('Tamas', '1985-01-19'), ('Peter', '1988-01-30'), ('Norbert', '2002-04-19');

INSERT INTO rent(visitor_id, book_id, rent_date)
	VALUES  (1, 1, '2002-01-01'),
			(2, 2, '2020-01-01'),
			(3, 3, '2020-03-01'),
			(1, 4, '2020-06-06'),
			(2, 5, '2020-05-01');

/*INSERT INTO books (title, year_of_issue, description, author_id, category, rentable, max_rent_time) 
		    VALUES ('szaz eves ember', 2009, 'kiraly konyv', 5, 'vigjatek', 1, '2 het');*/

/*UPDATE books SET title = 'new value' WHERE id = 12;*/

/*DELETE FROM books WHERE id = 11;*/

/*ALTER TABLE author ADD COLUMN phone VARCHAR(128);*/
/*describe author;*/