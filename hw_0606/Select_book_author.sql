
/*írjunk egy riportot, ahol listázzuk hogy melyik könyv (szerzővel), mikor kinek lett kiadva a kiadás évének sorrendjében*/
SELECT b.title, a.name AS author_name, v.name AS visitor_name, r.rent_date
FROM rent r
JOIN books b ON (b.id = r.book_id) /*ez kötelezően kitöltendő mező*/
JOIN visitor v ON (v.id = r.visitor_id)
LEFT JOIN books_author ba ON (ba.books_id = b.id)
LEFT JOIN author a ON (a.id = ba.author_id) /*ez nem kötelezően kitöltendő mező*/
ORDER BY r.rent_date;

/*melyik könyv nem volt még sosem kiadva*/
SELECT *
FROM books
WHERE id NOT IN (SELECT DISTINCT book_id FROM rent);

/*melyik könyv hányszor lett kiadva*/
SELECT books.title, count(*)
FROM rent
JOIN books ON books.id = rent.book_id
GROUP BY books.title;

/*melyik visitor nem kölcsönzött még könyvet sosem*/
SELECT name
FROM visitor
WHERE id NOT IN (SELECT visitor_id FROM rent);

/*ki a legidősebb szerző*/
SELECT *
FROM author
WHERE birth_year = (SELECT min(birth_year) FROM author);

/*melyik könyv lett idén kiadva*/
SELECT r.rent_date, v.name, b.title
FROM rent r
JOIN books b ON (b.id = r.book_id)
JOIN visitor v ON (v.id = r.visitor_id)
WHERE YEAR(r.rent_date) = YEAR(NOW());

/*angol írók könyvei*/
SELECT title, year_of_issue, description, name, country
FROM books
JOIN books_author ON books.id = books_author.books_id
JOIN author ON author.id = books_author.author_id
WHERE country = 'English';

/*legfiatalabb visitor*/
SELECT *
FROM visitor
WHERE birth_date = (SELECT max(birth_date) FROM visitor);

/*kategóriánként könyvek száma*/
SELECT categories.name, count(*)
FROM books
JOIN categories ON categories.id = books.category_id
GROUP BY category_id;

/*szerző nélküli könyvek listája*/
SELECT *
FROM books
LEFT JOIN books_author ON books.id = books_author.books_id
LEFT JOIN author ON author.id = books_author.author_id
WHERE author_id IS NULL;


/*HF*/
SELECT *
FROM books
LEFT JOIN books_author ON books.id = books_author.books_id
LEFT JOIN author ON author.id = books_author.author_id;