
package banksimulation.model;

import banksimulation.exceptions.BankingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bank {
    private List<Client> clients;
    private List<Transfer> transfers;
    private List<BankingException> bankingExceptions = new ArrayList<>();
    private Map<Client,Long> initialBalances;

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
        initialBalances = new HashMap<>();
        for(Client c: clients){
            initialBalances.put(c, c.getBalance());
        }
    }

    public List<Transfer> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<Transfer> transfers) {
        this.transfers = transfers;
    }

    public List<BankingException> getBankingExceptions() {
        return bankingExceptions;
    }

    public void setBankingExceptions(List<BankingException> bankingExceptions) {
        this.bankingExceptions = bankingExceptions;
    }
    
    public long getInitialAmountOfClient(Client client){
        return initialBalances.get(client);
    }
    
}
