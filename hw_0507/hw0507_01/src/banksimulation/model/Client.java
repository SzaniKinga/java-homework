package banksimulation.model;

public class Client {

    private String clientId;

    private String accountNumber;

    private long balance;

    public Client(String clientId, String accountNumber, long balance) {
        this.clientId = clientId;
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Client{" + "clientId=" + clientId + ", accountNumber=" + accountNumber + ", balance=" + balance + '}';
    }

}
