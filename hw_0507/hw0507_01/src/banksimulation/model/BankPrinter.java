/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banksimulation.model;

import banksimulation.exceptions.BankingException;
import java.util.List;

public class BankPrinter {

    private final Bank bank;

    public BankPrinter(Bank bank) {
        this.bank = bank;
    }

    public void printBalanceDelta() {
        System.out.println("Egyenlegváltozások adatai: ");
        for (Client c : bank.getClients()) {
            long difference = c.getBalance() - bank.getInitialAmountOfClient(c);
            System.out.println("Azonosító: " + c.getClientId());
            System.out.println("Kezdeti egyenleg: " + bank.getInitialAmountOfClient(c));
            System.out.println("Végső egyenleg: " + c.getBalance());
            System.out.println("Egyenlegváltozás: " + difference);
            System.out.println("----------------------------");
        }
    }

    public void printTransfers() {
        System.out.println("Utalások adatai: ");
        for (Transfer t : bank.getTransfers()) {
            System.out.print("Küldő: " + t.getSource().getClientId());
            System.out.print("\t | Kedvezményezett: " + t.getTarget().getClientId());
            System.out.print("\t | Összeg: " + t.getAmmount());
            System.out.print("\t | Teljesítés dátuma: " + t.getDateOfCompletion());
            System.out.println("");
        }
    }

    public void printErrors() {
        System.out.println("Hibalista: ");
        for (BankingException b : bank.getBankingExceptions()) {
            System.out.println(b.getMessage() + " |");
            System.out.print(b.getTransfer().toString());
            System.out.println("");
        }
    }

    public void printClientDetails(String balanceStatus) {
        System.out.println("Kliensek azonosítói és " + balanceStatus + " egyenlegei: ");
        for (Client c : bank.getClients()) {
            System.out.print("Azonosító: " + c.getClientId());
            System.out.print("\t | " + balanceStatus + " egyenleg: " + c.getBalance());
            System.out.println("");
        }
    }

}
