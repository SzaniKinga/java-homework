package banksimulation.exceptions;

import banksimulation.model.Transfer;

public class InsufficientFundsException extends BankingException {

    public InsufficientFundsException(Transfer transfer) {
        super("Nincs elég fedezet!", transfer);
    }

}
