package banksimulation.exceptions;

import banksimulation.model.Transfer;

public class IllegalTargetClientException extends BankingException {

    public IllegalTargetClientException(Transfer transfer) {
        super("A kedvezményezett nem egyezhet meg a küldővel!", transfer);
    }

}
