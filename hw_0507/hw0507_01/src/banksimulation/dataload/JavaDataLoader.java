package banksimulation.dataload;

import banksimulation.BankApplication;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JavaDataLoader implements DataLoader {

    @Override
    public List<Client> readClients() {
        List<Client> clients = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            clients.add(generateRandomClient());
        }
        return clients;
    }

    @Override
    public List<Transfer> readTransfers(List<Client> clients) {
        List<Transfer> transfers = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            transfers.add(generateTransfer(clients));
        }
        return transfers;
    }


    public static Transfer generateTransfer(List<Client> clients) {
        Transfer t = new Transfer();

        int randomSourceIndex = (int) (Math.random() * clients.size());
        int randomTargetIndex = (int) (Math.random() * clients.size());

        t.setSource(clients.get(randomSourceIndex));
        t.setTarget(clients.get(randomTargetIndex));

        long ammount = (long) (Math.random() * BankApplication.MAX_TRANSFER_AMOUNT);

        t.setAmmount(ammount);

        t.setDateOfCompletion(new Date());

        return t;
    }

    public static Client generateRandomClient() {
        long balance = (long) (Math.random() * 100000 + BankApplication.CLIENT_BASE_BALANCE);
        String clientId = generateRandomClientId();
        String accountNumber = generateRandomAccountNumber();

        Client c = new Client(clientId, accountNumber, balance);

        return c;
    }

    public static String generateRandomString(int length, int min, int max) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < length; i++) {
            char randomChar = (char) (Math.random() * (max - min + 1) + min);
            sb.append(randomChar);
        }

        return sb.toString();
    }

    public static String generateRandomClientId() {
        return generateRandomString(5, 65, 90);
    }

    public static String generateRandomAccountNumber() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 15; i++) {
            char randomChar = (char) (Math.random() * 46 + 48);
            sb.append(randomChar);
        }

        return generateRandomString(15, 48, 90);
    }

}
