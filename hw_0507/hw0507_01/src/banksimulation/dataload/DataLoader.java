package banksimulation.dataload;

import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.util.List;

public interface DataLoader {

    public List<Client> readClients();

    public List<Transfer> readTransfers(List<Client> clients);

}
