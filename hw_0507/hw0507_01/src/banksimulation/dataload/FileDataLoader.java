package banksimulation.dataload;

import banksimulation.BankApplication;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class FileDataLoader implements DataLoader {

    public List<Transfer> readTransfers(List<Client> clients) throws NumberFormatException {
        File transferFile = new File("transfers.txt");
        List<Transfer> transfers = new ArrayList<>();

        try (FileReader fr = new FileReader(transferFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] transferArray = line.split(",");

                String clientId1 = transferArray[0];
                String clientId2 = transferArray[1];
                long amount = Long.parseLong(transferArray[2]);

                Client source = getClientById(clients, clientId1);
                Client target = getClientById(clients, clientId2);

                Transfer t = new Transfer(source, target, amount, new Date());
                transfers.add(t);
                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }
        return transfers;
    }

    public List<Client> readClients() throws NumberFormatException {
        File clientFile = new File("clients.txt");
        List<Client> clients = new ArrayList<>();

        try (FileReader fr = new FileReader(clientFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] clientArray = line.split(",");

                String clientId = clientArray[0];
                String accountNumber = clientArray[1];
                long balance = Long.parseLong(clientArray[2]);

                Client c = new Client(clientId, accountNumber, balance);

                clients.add(c);

                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }
        return clients;
    }

    private Client getClientById(List<Client> clients, String clientId) {
        Optional<Client> optionalClient = filterClientsById(clients, clientId);
        if (optionalClient.isPresent()) {
            return optionalClient.get();
        } else {
            Client client = new Client(clientId, "", 0);
            return client;
        }
    }

    public Optional<Client> filterClientsById(List<Client> clients, String clientId) {
        return clients.stream().filter(x -> x.getClientId().equals(clientId)).findFirst();

    }

}
