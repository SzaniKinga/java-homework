/*Készíts egy banki utaló rendszert, ami szimulál személyek közötti átutalásokat.
Minden személynek legyen neve, bankszámlaszáma, és egyenlege. Egy utalásnak a 
következőket kell tartalmaznia: utaló személy, cél személy, átutalandó összeg, 
teljesítés dátuma.
Ha az utalást indítványozó személnyek nincs megfelelő egyenlege, akkor ezt 
kezeljük le egy saját kivételosztállyal (pl InsufficientFundsException).
A program véletlenszerűen generáljon le személyeket, és ezen személyek közötti 
utalásokat.
Írjuk ki a konzolra:
a.    a személyek neveit és kezdő egyenlegét
b.    az utalások adatait, dátum alapján növekvő sorrendben
c.    a személyek neveit és végső egyenlegét, egyenleg változásukat
d.    hibás utalásokat és miért voltak hibásak
Az előző rendszerünket módosítsuk úgy, hogy fájl alapú adatbetöltéssel illetve 
kiírással működjön. Több bank utalásait is tudja szimulálni, és ezeket 
rendszerezett módon írjuk ki.*/
package banksimulation;

import banksimulation.dataload.DataLoader;
import banksimulation.dataload.FileDataLoader;
import banksimulation.dataload.JavaDataLoader;
import banksimulation.exceptions.IllegalTargetClientException;
import banksimulation.exceptions.InsufficientFundsException;
import banksimulation.exceptions.BankingException;
import banksimulation.model.Bank;
import banksimulation.model.BankPrinter;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BankApplication {

    public static final int CLIENT_BASE_BALANCE = 200000;
    public static final int MAX_TRANSFER_AMOUNT = 200000;

    public static void main(String[] args) {

        //DataLoader loader = new JavaDataLoader();
        DataLoader loader = new FileDataLoader();

        Bank bank1 = new Bank();
        bank1.setClients(loader.readClients());
        bank1.setTransfers(loader.readTransfers(bank1.getClients()));
        
        BankPrinter bp = new BankPrinter(bank1);

        bp.printClientDetails("kezdő");

        bp.printTransfers();

        simulate(bank1.getTransfers(), bank1.getBankingExceptions());

        bp.printClientDetails("végső");
        
        bp.printBalanceDelta();

        bp.printErrors();
    }

   
    public static void simulate(List<Transfer> transfers, List<BankingException> bankingExceptions) {
        for (Transfer t : transfers) {
            try {
                simulateTransfer(t);
            } catch (BankingException e) {
                bankingExceptions.add(e);
            }
        }
    }

    public static void simulateTransfer(Transfer t) throws InsufficientFundsException, IllegalTargetClientException {
        Client source = t.getSource();
        Client target = t.getTarget();

        long amount = t.getAmmount();

        long sourceBalance = source.getBalance();
        long targetBalance = target.getBalance();
        
        if (source.equals(target)) {
            throw new IllegalTargetClientException(t);
        } 
        
        if (sourceBalance >= amount) {
            source.setBalance(sourceBalance - amount);
            target.setBalance(targetBalance + amount);
        } else {
            throw new InsufficientFundsException(t);
        }

        
    }

}
