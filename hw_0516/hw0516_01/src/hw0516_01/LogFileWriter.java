/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw0516_01;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class LogFileWriter {
    
    private File f;

    public LogFileWriter(String fileName) {
        f = new File(fileName);
        f.delete();
    }
    
    public void write(String line) {
        
        try (FileWriter fw = new FileWriter(f, true);
                BufferedWriter bw = new BufferedWriter(fw);) {

            bw.write(line + "\n");
        } catch (IOException ex) {
            System.err.println("Fájlhiba");
            ex.printStackTrace();
        }

        
    }

}
