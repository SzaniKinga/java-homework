/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw0516_01;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LogModel {

    private int lineCount = 0;
    private Date previousDate;

    public String getLogText() {
        Date date = new Date();
        StringBuilder sb = new StringBuilder();
        if (previousDate != null && previousDate.getHours() != date.getHours()) {
            sb.append("\n");
        }
        sb.append(++lineCount);
        sb.append(". [");
        sb.append(formatDate(date));
        sb.append("]: Button pressed");
        previousDate = date;

        return sb.toString();
    }

    private String formatDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return formatter.format(date);
    }

}
