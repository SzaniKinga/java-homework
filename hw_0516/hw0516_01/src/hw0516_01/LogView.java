/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw0516_01;

import java.awt.BorderLayout;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class LogView extends JFrame {
    
    LogController controller;
    JLabel label = new JLabel("Time log");
    JTextArea area = new JTextArea();
    JButton button = new JButton("Log");

    public LogView(LogController controller) {
        this.controller = controller;
    }
    
    
    public void init(){
        setVisible(true); 
        setSize(400, 500); 
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null); 
        label.setHorizontalAlignment(JLabel.CENTER);

        add(label, BorderLayout.NORTH);
        add(area, BorderLayout.CENTER);
        add(button, BorderLayout.SOUTH);
        
        button.addActionListener(event -> controller.log());
       
    }

    public void appendToTextArea(String text) {
        area.append(text + "\n");
    }
    
    
    
    
}
