/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw0516_01;

public class LogController {
    
    private LogView logView;
    private LogModel logModel;
    private LogFileWriter logFileWriter;
    
    public LogController() {
        this.logView = new LogView(this);
        this.logModel = new LogModel();
        this.logFileWriter = new LogFileWriter("log.txt");
        logView.init();
    }

    public void log() {
        String logText = logModel.getLogText();
        this.logView.appendToTextArea(logText);
        this.logFileWriter.write(logText);
        
    }
    
 

}
