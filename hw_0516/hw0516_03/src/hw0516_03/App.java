/*Készíts egy napok ENUM-ot, ahol minden nap lehet egy sayHello üzenetet kiíratni, 
ami minden napnál más, és az átlagos aktuális hangulatot képviseli.*/
package hw0516_03;

public class App {

    public static void main(String[] args) {

        for (Weekday day : Weekday.values()) {
            System.out.println(day.sayHello());
        }

    }
}
