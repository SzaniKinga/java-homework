/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw0516_03;

public enum Weekday {
    
    MONDAY{
        @Override public String sayHello(){ 
                return "Megint egy ébredés, de nem haragszom én rád hétfő... :/";
        }
    }, 
    TUESDAY{
        @Override public String sayHello(){ 
                return "Miért vagyok ilyen keddvetlen??";
        }
    }, 
    WEDNESDAY{
        @Override public String sayHello(){ 
                return "Kábítószerda";
        }
    }, 
    THURSDAY{
        @Override public String sayHello(){ 
                return "Kispéntek";
        }
    }, 
    FRIDAY{
        @Override public String sayHello(){ 
                return "TGIF! Ma berúgunk! :)";
        }
    }, 
    SATURDAY{
        @Override public String sayHello(){ 
                return "Mivót tegnap? Nem emlékszem teljesen...";
        }
    }, 
    SUNDAY{
        @Override public String sayHello(){ 
                return "Afene, holnap megint hétfő :(";
        }
    };
    

    public String sayHello(){
        return "";
    }
    
    
}
