package bh0516_04.service;

public class CalculatorService implements CalculatorServiceInterface {

    public int addNumbers(int n1, int n2) {
        return n1 + n2;
    }

    public int multiplyNumbers(int n1, int n2) {
        return n1 * n2;
    }

}
