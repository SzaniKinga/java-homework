/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0516_04.service;

public interface CalculatorServiceInterface {

    public int addNumbers(int n1, int n2);

    public int multiplyNumbers(int n1, int n2);
    
}
