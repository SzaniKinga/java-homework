/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0516_04.model;

public class CalculatorModel {
    
    private int displayedNumber = 0;
    private int numberInMemory = 0;
    private Operation operations;

    public int getDisplayedNumber() {
        return displayedNumber;
    }

    public void setDisplayedNumber(int displayedNumber) {
        this.displayedNumber = displayedNumber;
    }

    public int getNumberInMemory() {
        return numberInMemory;
    }

    public void setNumberInMemory(int numberInMemory) {
        this.numberInMemory = numberInMemory;
    }

    public Operation getOperations() {
        return operations;
    }

    public void setOperations(Operation operations) {
        this.operations = operations;
    }
    
    
    
}
