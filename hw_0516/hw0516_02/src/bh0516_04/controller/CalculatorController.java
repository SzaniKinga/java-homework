package bh0516_04.controller;

import bh0516_04.model.CalculatorModel;
import bh0516_04.model.Operation;
import bh0516_04.service.CalculatorServiceInterface;
import bh0516_04.view.CalculatorView;

public class CalculatorController {

    private CalculatorModel model;
    private CalculatorView view;
    private CalculatorServiceInterface calculatorService;
    
    public CalculatorController(CalculatorServiceInterface calculatorService) {
        this.model = new CalculatorModel(); 
        this.view = new CalculatorView(this); 
        this.view.init();
        this.calculatorService = calculatorService;
    }

    public void handleClearButtonClick() {
        model.setDisplayedNumber(0);
        model.setNumberInMemory(0);
        model.setOperations(null);

        updateViewFromModel();
    }

    public void handleNumberButtonClick(String str) {
        String newButtonText = model.getDisplayedNumber() + str;
        model.setDisplayedNumber(Integer.valueOf(newButtonText));

        updateViewFromModel();
    }

    public void handlePlusButton() {
        this.model.setNumberInMemory(this.model.getDisplayedNumber());
        this.model.setOperations(Operation.ADDITION);
        this.model.setDisplayedNumber(0);

        updateViewFromModel();
    }

    public void handleEqualsButton() {
        if (this.model.getOperations() != null) {
            switch (this.model.getOperations()) {
                case ADDITION:
                    int calcResult = calculatorService.addNumbers(this.model.getDisplayedNumber(),this.model.getNumberInMemory());
                    this.model.setNumberInMemory(0);
                    this.model.setDisplayedNumber(calcResult);
                    break;
            }
            this.model.setOperations(null);

            updateViewFromModel();
        }
    }

    public void updateViewFromModel() {
        view.setScreenText(String.valueOf(model.getDisplayedNumber()));
    }

}
