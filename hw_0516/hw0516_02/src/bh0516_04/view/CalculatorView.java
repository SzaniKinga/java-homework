package bh0516_04.view;

import bh0516_04.controller.CalculatorController;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalculatorView extends JFrame {

    private CalculatorController controller;
    private JPanel panel;
    private JTextField screen;
    private JButton plusButton;
    private JButton equalButton;
    private JButton clearButton;
    private JButton zeroButton;
    private JButton[] numberButtons = new JButton[9];

    public CalculatorView(CalculatorController controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();
        setUpCalculatorScreen();
        setUpEqualButton();
        setUpPlusButton();
        setUpClearButton();
        setUpNumbersButtons();
        showWindow();
    }

    private void setUpWindow() { 
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("Calculator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setLayout(new GridBagLayout());

        add(panel);
    }

    private void showWindow() {
        this.setVisible(true);
    }

    private void setUpCalculatorScreen() {
        this.screen = new JTextField("0");
        Font f = new Font("Arial", Font.BOLD, 20);
        this.screen.setFont(f); 
        this.screen.setHorizontalAlignment(JTextField.RIGHT); 

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0; 
        gbc.gridy = 0; 
        gbc.gridwidth = 5;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.BOTH; 
        panel.add(this.screen, gbc);

    }

    private void setUpEqualButton() {
        this.equalButton = new JButton("=");
        this.equalButton.addActionListener(event -> {
            controller.handleEqualsButton();
        });
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 2; 
        gbc.gridx = 4;
        gbc.gridy = 3;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(this.equalButton, gbc);
    }

    private void setUpPlusButton() {
        this.plusButton = new JButton("+");
        this.plusButton.addActionListener(event -> {
            controller.handlePlusButton();
        });
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 4;
        gbc.gridy = 2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(this.plusButton, gbc);
    }

    private void setUpClearButton() {
        this.clearButton = new JButton("C");
        this.clearButton.addActionListener(event -> {
            controller.handleClearButtonClick();
        });
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 4;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(this.clearButton, gbc);
    }

    private void setUpNumbersButtons() {
        this.zeroButton = new JButton("0");
        GridBagConstraints gbc = new GridBagConstraints();
        ActionListener numberListener = event -> {
            JButton jb = (JButton) event.getSource();
            controller.handleNumberButtonClick(jb.getText());
        };
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 3;
        gbc.fill = GridBagConstraints.BOTH;

        panel.add(zeroButton, gbc);

        int num = 1;

        for (int i = 3;
                i >= 1; i--) {
            for (int j = 1; j < 4; j++) { 
                JButton button = new JButton(Integer.toString(num));
                button.addActionListener(numberListener);
                gbc.gridx = j;
                gbc.gridy = i;
                gbc.weightx = 1;
                gbc.weighty = 1;
                gbc.gridwidth = 1;
                gbc.fill = GridBagConstraints.BOTH;
                panel.add(button, gbc);

                this.numberButtons[num - 1] = button;
                num++;

            }
        }
    }

    public void setScreenText(String screenText) {
        this.screen.setText(screenText);
    }

}
