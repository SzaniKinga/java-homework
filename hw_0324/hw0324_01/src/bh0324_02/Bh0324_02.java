/* Keszitsen egy kezdetleges aknakeresot! A palyameretet lehessen paranccsori 
argumentbol megadni. NxM-es legyen a palyameret. A palyameret teruletenek minimum 
negyedere maximum felere generaljon aknakat. A jatekos kezdjen tippelni 
koordinatakra a palyan belul. A program jelezze ha aknara lepett a felhasznalo 
ellenkezo esetben irja ki, tulelted. Ilyenkor a jatek veget er es a program 
kiirja mennyi mezot sikerult lepni robbanas nelkul. Plusz feladat: Amikor a 
felhasznalo tippel egyet, jelezze a felhasznalonak, hogy mennyi akna van korulotte. 
Ha 1 akna van korulotte irja ki: Nincs para csak 1 akna van korulotted. Ha 2-4: 
Ovatosan kezd melegedni a helyzet. Ha 4-nel tobb akkor irja ki: Hurt Locker!!
Plusz feladat: Legyen paranccsori argumentumbol megadhato, hogy mennyi elete van 
a felhasznalonak. Aknaralepesnel egy eleteropontot veszit a jatekos. Akkor er 
veget ebben az esetben a jatek ha a jatekos osszes eleteropontja elfogyott.
 */
package bh0324_02;

import java.util.Random;
import java.util.Scanner;

public class Bh0324_02 {

    public static void main(String[] args) {
   
        int mapSizeX = Integer.parseInt(args[0]);
        int mapSizeY = Integer.parseInt(args[1]);
        boolean[][] map = new boolean[mapSizeX][mapSizeY];
        
        generateMines(map);
        
        int livesNr = Integer.parseInt(args[2]);
        System.out.println("Az életeid száma: " + livesNr);

        for (int outer = 0; outer < map.length; ++outer) {
            for (int inner = 0; inner < map[outer].length; ++inner) {
                System.out.print(map[outer][inner] + " ");
            }
            System.out.println(" ");
        }
        
        do {
        Scanner sc = new Scanner(System.in);
        System.out.println("Kérlek, add meg az X koordinátát!");
        int coordX = sc.nextInt();
        System.out.println("Kérlek, add meg az Y koordinátát!");
        int coordY = sc.nextInt();
        printMineWarningMessage(getNearbyMines(map, coordX, coordY));

        if (map[coordX][coordY] == true) {
        livesNr = livesNr - 1;
        System.out.println("Aknára léptél, egy életed elveszett!");
        System.out.println("A megmaradt életeid száma: " + livesNr);
        }
        else {
           System.out.println("Túlélted!"); 
        }
        } while (livesNr != 0);
        System.out.println("Sajnos vesztettél!"); 
        
    }

    public static void generateMines(boolean[][] map) {
        int rows = map.length;
        int columns = map[0].length;
        for (int counter = 0 ; counter < amountOfMinesToGenerate(map) ; ++counter) {
            int generatedRowNum;
            int generatedColNum;
            do {
                generatedRowNum = new Random().nextInt(rows);
                generatedColNum = new Random().nextInt(columns);
            } while (map[generatedRowNum][generatedColNum]);
            map[generatedRowNum][generatedColNum] = true;
        }
    }
    public static int amountOfMinesToGenerate(boolean[][] map) {
        int rows = map.length;
        int columns = map[0].length;
        int area = rows * columns;
        int min = (int) (area * 0.25);
        int max = (int) (area * 0.5);
        return new Random().nextInt(max - min + 1) + min;
    }
    public static void printMineWarningMessage(int neighbouringMines) {
        if (neighbouringMines == 0) {
            System.out.println("Semmi gond. Nincs akna a közelben.");
        } else if (neighbouringMines == 1) {
            System.out.println("Nincs para, csak egy akna van körülötted.");
        } else if (neighbouringMines > 1 && neighbouringMines < 5) {
            System.out.println("Vigyázat! 2-4 akna van körülötted!");
        } else {
            System.out.println("Hurt Locker! Négynél is több akna vesz körül!");
        }
    }
    public static int getNearbyMines(boolean[][] map, int currentPosX, int currentPosY) {
        int MIN_X = 0;
        int MIN_Y = 0;
        int MAX_X = map.length-1;
        int MAX_Y = map[0].length-1;
        int startPosX = (currentPosX - 1 < MIN_X) ? currentPosX : currentPosX - 1;
        int startPosY = (currentPosY - 1 < MIN_Y) ? currentPosY : currentPosY - 1;
        int endPosX = (currentPosX + 1 > MAX_X) ? currentPosX : currentPosX + 1;
        int endPosY = (currentPosY + 1 > MAX_Y) ? currentPosY : currentPosY + 1;
        int neighbouringMines = 0;
        for (int rowNum = startPosX; rowNum <= endPosX; rowNum++) {
            for (int colNum = startPosY; colNum <= endPosY; colNum++) {
                if (map[rowNum][colNum] == true) {
                    ++neighbouringMines;
                }
            }
        }
        return neighbouringMines;
    }
}  
