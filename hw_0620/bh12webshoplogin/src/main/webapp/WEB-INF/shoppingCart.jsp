<%-- 
    Document   : shoppingCart
    Created on : 2020.06.22., 21:35:09
    Author     : Kinga
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
        <c:forEach var="product" items="${pl}">
            <tr>
            <td>${product.name}</td>
            <td>${product.price}</td>
            </tr>
        </c:forEach>
        </table>
        <a href="ProductListServlet">Back to product list</a>
    </body>
</html>