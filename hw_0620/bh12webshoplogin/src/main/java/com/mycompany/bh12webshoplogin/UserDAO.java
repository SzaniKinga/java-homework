/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bh12webshoplogin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAO extends AbstractDao {
    private static final String SELECT_USER_ID_BY_EMAIL = "SELECT id FROM appuser WHERE email=?";
    
    public int getUserIdByEmail(String email) {
         try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
         
        try (Connection conn = DriverManager.getConnection(getConnertionUrl(), "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(SELECT_USER_ID_BY_EMAIL);) {
            
            ps.setString(1, email);

            final ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                return rs.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ShoppingCartServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return 0;
    }
}
