/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bh12webshoplogin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductDao extends AbstractDao implements Dao<Product> {

    private static final String INSERT_QUERY = "INSERT INTO product (productName, price) values(?, ?)";
    private static final String SELECT_ALL_QUERY = "SELECT * FROM product";

    @Override
    public void save(Product p) {
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        
        try (Connection conn = DriverManager.getConnection(getConnertionUrl(), "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(INSERT_QUERY);) {

            ps.setString(1, p.getName());
            ps.setInt(2, p.getPrice());

            int rows = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(ShoppingCartServlet.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }

    @Override
    public List<Product> getAll() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }

        List<Product> products = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(getConnertionUrl(), "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(SELECT_ALL_QUERY);) {

            final ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                final int id = rs.getInt(1);
                final String productName = rs.getString(2);
                final int price = rs.getInt(3);
                Product p = new Product(productName, price);
                p.setId(id);
                products.add(p);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ShoppingCartServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products;
    }

}
