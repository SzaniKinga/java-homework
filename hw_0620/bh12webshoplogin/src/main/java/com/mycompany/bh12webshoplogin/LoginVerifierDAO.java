/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bh12webshoplogin;

import at.favre.lib.crypto.bcrypt.BCrypt;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginVerifierDAO {

    private static final String CONNECTION_URL = "jdbc:mysql://localhost/webshopLibrary?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String QUERY_USER = "SELECT * FROM appuser WHERE appuser.email = ?";

    private LoginVerifierDAO() {
    }

    private static final LoginVerifierDAO INSTANCE = new LoginVerifierDAO();

    public static LoginVerifierDAO getInstance() {
        return INSTANCE;
    }

    public boolean login(AppUserDTO appUserDTO) throws ClassNotFoundException {
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, "admin", "admin");
                PreparedStatement ps = connection.prepareStatement(QUERY_USER)) {

            ps.setString(1, appUserDTO.getEmail());
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                String hashedPassword = rs.getString("pass");
                final BCrypt.Result result = BCrypt.verifyer().verify(appUserDTO.getPassword().toCharArray(), hashedPassword);
                return result.verified;
            }
            return false;
            
        } catch (SQLException ex) {
            Logger.getLogger(LoginVerifierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }
}
