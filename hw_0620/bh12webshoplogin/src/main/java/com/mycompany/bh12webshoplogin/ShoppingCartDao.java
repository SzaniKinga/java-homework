/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bh12webshoplogin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShoppingCartDao extends AbstractDao {
    
    private static final String INSERT_QUERY = "INSERT INTO shoppingCart (appuser_id, product_id) values(?, ?)";
    private static final String LIST_QUERY = "SELECT * FROM product WHERE id IN (SELECT product_id FROM shoppingCart WHERE appuser_id = ?)";
    
    public void addProduct(int productId, int userId) {
         try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        
        try (Connection conn = DriverManager.getConnection(getConnertionUrl(), "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(INSERT_QUERY);) {
            
            ps.setInt(1, userId);
            ps.setInt(2, productId);

            int rows = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(ShoppingCartServlet.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }

    
    public List<Product> getAll(int userId) {
        
        List<Product> products = new ArrayList<>();
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        
        try (Connection conn = DriverManager.getConnection(getConnertionUrl(), "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(LIST_QUERY);) {

            ps.setInt(1, userId);

            final ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                final int id = rs.getInt(1);
                final String productName = rs.getString(2);
                final int price = rs.getInt(3);
                Product p = new Product(productName, price);
                p.setId(id);
                products.add(p);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ShoppingCartServlet.class.getName()).log(Level.SEVERE, null, ex);
        }    
        
        return products;
    }
}
