/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bh12webshoplogin;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ShoppingCartServlet", urlPatterns = {"/ShoppingCartServlet"})
public class ShoppingCartServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Integer userId = (Integer)(req.getSession().getAttribute("userId"));
        if (req.getParameterMap().keySet().contains("list")) {
            List<Product> pl = new ShoppingCartDao().getAll(userId);
            req.setAttribute("pl", pl);
            req.getRequestDispatcher("WEB-INF/shoppingCart.jsp").forward(req, res);
        } else {
            int productId = Integer.valueOf(req.getParameter("product_id"));
            ShoppingCartDao sd = new ShoppingCartDao();
            sd.addProduct(productId, userId);
            ProductDao pd = new ProductDao();
            req.setAttribute("pl", pd);

            req.getRequestDispatcher("WEB-INF/productList.jsp").forward(req, res);
        }
    }


}
