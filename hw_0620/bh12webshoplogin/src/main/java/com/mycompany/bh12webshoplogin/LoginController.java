/*Csinaljatok egy webaruhazat. Lehessen belogolni. Majd belogolas utan lehessen 
termekeket felvenni egy shopping cart-ba. Es ennek a shopping cart tartalmat tudjuk listazni.
A termekeknek legyen neve es ara.
Bonusz: Egy mentes hatasara lehessen ezt a lista tartalmat a DB-be lehessen menteni, 
az adott bejelentkezett userhez egy termekek tablaba*/
package com.mycompany.bh12webshoplogin;

import bean.LoginBean;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import util.SessionUtil;

@WebServlet(name = "LoginController", urlPatterns = {"/LoginController"})
public class LoginController extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        LoginBean loginBean = (LoginBean) session.getAttribute("loginBean");
        if(loginBean == null){
            loginBean = new LoginBean();
            session.setAttribute("loginBean", loginBean);
        }
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LoginBean loginBean = SessionUtil.getLoginPageBean(req);
        if(loginBean.isIsValidLogin() && "true".equals(req.getParameter("reset"))){
            loginBean.reset();
        }
        req.getRequestDispatcher("WEB-INF/login.jsp").forward(req,resp);
        
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            LoginBean loginBean = SessionUtil.getLoginPageBean(req);
            AppUserDTO appUserDTO = new AppUserDTO(req.getParameter("email"), req.getParameter("password"));
            loginBean.setAttempts(loginBean.getAttempts() + 1);

        try {
            if (LoginVerifierDAO.getInstance().login(appUserDTO)){
                loginBean.setIsInvalidLogin(false);
                loginBean.setIsValidLogin(true);
                HttpSession session = req.getSession();
                UserDAO ud = new UserDAO();
                session.setAttribute("userId", ud.getUserIdByEmail(req.getParameter("email")));
                resp.sendRedirect("ProductListServlet"); 
            } else {
                loginBean.setIsInvalidLogin(true);
                loginBean.setIsValidLogin(false);
                resp.sendRedirect(req.getRequestURI());
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
   

       
    }
    
}