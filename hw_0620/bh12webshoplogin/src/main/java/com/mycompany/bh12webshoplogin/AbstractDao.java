/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bh12webshoplogin;

public abstract class AbstractDao {
    
    private static final String CONNECTION_URL = "jdbc:mysql://localhost/webshopLibrary?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    
    public String getConnertionUrl(){
        return CONNECTION_URL;
    }
    
}
