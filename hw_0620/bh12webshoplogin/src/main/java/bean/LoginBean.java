/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import java.util.List;

public class LoginBean implements Serializable {
    
    private boolean isValidLogin;
    private boolean isInvalidLogin;
    private int attempts;
    private List<String> list;

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }
    
    
    public void reset(){
        isValidLogin = false;
        isInvalidLogin = false;
        attempts = 0;
    }

    public boolean isIsValidLogin() {
        return isValidLogin;
    }

    public void setIsValidLogin(boolean isValidLogin) {
        this.isValidLogin = isValidLogin;
    }

    public boolean isIsInvalidLogin() {
        return isInvalidLogin;
    }

    public void setIsInvalidLogin(boolean isInvalidLogin) {
        this.isInvalidLogin = isInvalidLogin;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    @Override
    public String toString() {
        return "LoginBean{" + "isValidLogin=" + isValidLogin + ", isInvalidLogin=" + isInvalidLogin + ", attempts=" + attempts + '}';
    }
    
    
}
