<%-- 
    Document   : car
    Created on : 2020.06.16., 17:39:23
    Author     : Kinga
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<Context antiJARlocking ="true" path ="/*"/>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login page</title>
    </head>
    <body>
        <h1>Please login:</h1>
        ${loginBean}
        <br>
        <c:if test ="${loginBean.isValidLogin}">
            <br>
            Attempts: <c:out value = "${loginBean.attempts}"/>
            <br>
            <a href = "?reset=true">Restart...</a>
        </c:if>
        <c:if test ="${!loginBean.isValidLogin}">
            <form method="post">
                Email:<br>
                <input type="text" name="email">
                <br>
                Password:<br>
                <input type="password" name="password">
                <br><br>
                <input type="submit" value="Login">
            </form>
        </c:if>
        <c:if test ="${loginBean.isInvalidLogin}">
            <div style="color:red">
                <p>Hibás bejelentkezés!</p>
            </div>
            <c:out value = "${loginBean.attempts}"/>
        </c:if>
    </body>
</html>
