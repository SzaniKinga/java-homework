<%-- 
    Document   : productList
    Created on : 2020.06.21., 20:10:00
    Author     : Kinga
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
        <c:forEach var="product" items="${pl.all}">
            <tr>
            <td>${product.name}</td>
            <td>${product.price}</td>
            <td><a href="ShoppingCartServlet?product_id=${product.id}">Add to cart</a></td>
            </tr>
        </c:forEach>
        </table>
        <a href="ShoppingCartServlet?list=1">List cart</a>
    </body>
</html>
