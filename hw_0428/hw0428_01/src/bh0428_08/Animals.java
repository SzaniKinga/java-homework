/*Olvassuk be az „animals.csv” fájlt. Ebben található állatokról készítsünk statisztikát:
a.    Írjuk ki hány állatot tartalmaz a fájl, illetve hány tulajdonságot (a nevet ne számoljuk bele)
b.    Írjuk ki hány állatnak van szőre, hánynak nincs
c.    Írjuk ki hány állatnak van tolla, hánynak nincs
d.    Írjuk ki az adott lábszámhoz tartozó állatok darabszámát
e.    Írjuk ki a class_type alapján a típusokhoz tartozó állatok darabszámát
f.    Írjuk ki az összes nem lélegző állat nevét
g.    Írjuk ki azon állatok neveit és az összes tulajdonságait, amik gerinces és ragadózó állatok.
 */
package bh0428_08;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Animals {

    private static Map<String, String>[] animals = new Map[0];
    private static int numberofAnimalProperties;

    private static final String FILENAME = "animals.csv";

    public static void main(String[] args) {
        List<String> list = readFile(FILENAME);
        extractAnimals(list);
        System.out.println("Number of animals: " + getNumberOfAnimals());
        System.out.println("Number of animal properties: " + getNumberOfAnimalProperties());
        System.out.println("Number of hairy animals: " + getNumberOfAnimalsWithProperty("hair"));
        System.out.println("Number of not hairy animals: " + getNumberOfAnimalsWithoutProperty("hair"));
        System.out.println("Number of animals with feather: " + getNumberOfAnimalsWithProperty("feathers"));
        System.out.println("Number of animals without feather: " + getNumberOfAnimalsWithoutProperty("feathers"));
        System.out.println("\nAnimals by leg count");
        printAnimalGroupCount("legs");
        System.out.println("\nAnimals by class_type");
        printAnimalGroupCount("class_type");
        System.out.println("\nNon breathing aimals:");
        printNamesOfNonBreathingAnimals();
        System.out.println("\nVertibrate predators:");
        printVertibrtePredators();
    }

    private static List<String> readFile(String fileName) {
        StringBuilder sb = new StringBuilder();
        String strLine;
        List<String> list = new LinkedList<>();

        try (FileReader fr = new FileReader(fileName);
                BufferedReader br = new BufferedReader(fr);) {

            while ((strLine = br.readLine()) != null) {
                sb.append(strLine);
                sb.append(System.lineSeparator());
                if (strLine != null) {
                    list.add(strLine);
                }
            }
            br.close();
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        } catch (IOException e) {
            System.err.println("Unable to read the file.");
        }

        return list;
    }

    private static void extractAnimals(List<String> list) {
        int i = 0;
        String[] header = null;
        for (String line : list) {
            String[] lineData = line.split(";");
            if (i == 0) {
                header = lineData;
                numberofAnimalProperties = header.length;
                animals = new Map[list.size() - 1];
            } else {
                animals[i - 1] = new HashMap<>();
                for (int j = 0; j < lineData.length; j++) {
                    animals[i - 1].put(header[j], lineData[j]);
                }
            }

            i++;
        }
    }

    private static int getNumberOfAnimals() {
        return animals.length;
    }

    private static int getNumberOfAnimalProperties() {
        return numberofAnimalProperties;
    }

    private static int getNumberOfAnimalsWithProperty(String property) {
        Map<String, Integer> groups = getAnimalCountGroupedByProperty(property);
        
        Integer count = groups.get("0");
        
        return count == null ? 0 : count;
    }

    private static int getNumberOfAnimalsWithoutProperty(String property) {
        return getNumberOfAnimals() - getNumberOfAnimalsWithProperty(property);
    }
    
    private static Map<String, Integer> getAnimalCountGroupedByProperty(String property) {
        Map<String, Integer> groups = new HashMap<>();
        Integer count;
        for (Map animal : animals) {
            if (animal == null) {
                continue;
            }
            
            String propertyValue = (String)animal.get(property);
            count = groups.get(propertyValue);
            if (count == null) {
                count = 0;
            }
            
            groups.put(propertyValue, count + 1);
        }
        
        return groups;
    }
    
    private static void printNamesOfNonBreathingAnimals() {
        for (Map animal : animals) {
            if (animal == null) {
                continue;
            }
            if (!isAnimal(animal, "breathes")) {
                String name = (String) animal.get("animal_name");
                if (name != null) {
                    System.out.println(name);
                }
            }
        }
    }
    
    private static void printAnimalGroupCount(String property) {
        Map<String, Integer> groups = getAnimalCountGroupedByProperty(property);
        groups.entrySet().forEach((entry) -> {
            System.out.println(entry.getKey() +
                    ": " + entry.getValue()); 
        });
    }

    private static void printVertibrtePredators() {
        for (Map animal : animals) {
            if (animal == null) {
                continue;
            }
            if (isAnimal(animal, "backbone") && isAnimal(animal, "predator")) {
                String name = (String) animal.get("animal_name");
                if (name != null) {
                    System.out.println(name);
                }
            }
        }
    }
    
    private static boolean isAnimal(Map<String, String> animal, String property) {
        String value = animal.get(property);
        if (value == null) {
            return false;
        }
        
        return value.equals("1");
    }
}
