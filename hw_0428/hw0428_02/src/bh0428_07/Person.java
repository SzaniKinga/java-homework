/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0428_07;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Person implements Comparable {
    private final String name;
    private final int age;
    private final Map<Currency, Double> wallet = new HashMap<>();

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    
    public void earn(Currency currency, double amount) {
        double currentAmount = wallet.containsKey(currency) ? wallet.get(currency) : 0;
        wallet.put(currency, currentAmount + amount);
    }
        
    public String getName() {
        return name;
    }
    
     public int getAge() {
        return age;
    }
    
    public double getWealth()
    {
        double wealth = 0;
        for (Map.Entry<Currency, Double> currency : wallet.entrySet()) {
            wealth += currency.getValue();
        }
        
        return wealth;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + ", wealth=" + getWealth() + "}";
    }

    
    @Override
    public int compareTo(Object o) {
        if (getWealth() == ((Person) o).getWealth()) {
            return 0;
        }
        return getWealth() > ((Person) o).getWealth() ? 1 : -1;
    } 

}
