package bh0428_07;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class People {

    private final List<Person> people = new ArrayList<>();

    public void addPerson(Person person) {
        people.add(person);
    }

    public double getTotalWealth() {
        double totalWealth = 0;
        for (Person person : people) {
            totalWealth += person.getWealth();
        }

        return totalWealth;
    }

    public double getAverageWealth() {
        return getTotalWealth() / people.size();
    }

    public double getAgeFilteredAverageWealth(int minAge, int maxAge) {
        People filteredPeople = filterAge(minAge, maxAge);

        return filteredPeople.getAverageWealth();
    }

    public double getMedianWealth() {
        Collections.sort(people);
        int middle = people.size() / 2;
        middle = middle > 0 && middle % 2 == 0 ? middle - 1 : middle;
        double median = 0;
        for (Person person : people) {
            median = (double) people.get(middle).getWealth();
        }
        return median;
    }

    private People filterAge(int minAge, int maxAge) {
        People filteredPeople = new People();
        for (Person person : people) {
            if (person.getAge() >= minAge && person.getAge() <= maxAge) {
                filteredPeople.addPerson(person);
            }
        }

        return filteredPeople;
    }
    
    public void sortWealth(){
        Collections.sort(people);
        System.out.println(people);
    }

}
