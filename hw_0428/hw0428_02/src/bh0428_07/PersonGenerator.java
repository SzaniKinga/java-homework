/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0428_07;

import java.util.Random;

public class PersonGenerator {
    private static final String[] NAMES = {"Kinga", "Andris", "István", "Józsi", "Timi"};
    private static final Random RND = new Random();
    
    private Person person;
    
    public People generateRandomPeople() {
        People people = new People();
        int limit = RND.nextInt(5)+5;
        for (int i = 0; i < limit; i++) {
            people.addPerson(generateRandomPerson());
        }
        
        return people;
    }
            
    private Person generateRandomPerson() {
        person = new Person(getRandomName(), getRandomAge());
        
        earnRandomCurrencies();
        
        return person;
    }
    
    private String getRandomName()
    {
        return NAMES[RND.nextInt(NAMES.length)];
    }
    
    private int getRandomAge() {
        return RND.nextInt(65) + 14;
    }
    
    private void earnRandomCurrencies() {
        for (Currency currency: Currency.values()) {
            if (RND.nextInt(2) == 1) {
                person.earn(currency, RND.nextInt(100));
            }
        }
        
    }
    
}
