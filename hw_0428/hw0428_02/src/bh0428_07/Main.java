/*Hozz létre egy Currency enumot. Tároljuk el benne a pénznemet (EUR, GBP, HUF, 
USD, JPN, RMB) és a forintbeli értékét integerként. Hozzunk létre egy Person 
osztályt is, amiben minden személynél eltároljuk, hogy melyik pénznemből hány 
darab van neki. Tároljuk el még a személy korát és nevét is.
Generáljunk véletlenszerűen embereket különböző típusú és mennyiségű pénzzel. 
Írjuk ki a konzolra a következőket:
a.    az emberek vagyonának összértékét
b.    az emberek vagyonának átlagát
c.    az emberek vagyonának átlagát 18-49 korosztályban
d.    az emberek vagyonának mediánját
e.    az emberek nevét, vagyonát növekvő sorrendben*/
package bh0428_07;

import java.awt.BorderLayout;

public class Main {

    public static void main(String[] args) {
        PersonGenerator generator = new PersonGenerator();
        People people = generator.generateRandomPeople();
        
        System.out.println("Total wealth: " + people.getTotalWealth());
        System.out.println("Average wealth: " + people.getAverageWealth());
        System.out.println("Average wealth of aged 18-49: " + people.getAgeFilteredAverageWealth(18, 49));
        System.out.println("Median wealth: " + people.getMedianWealth());
        System.out.println("People sorted according to their wealth: ");
        people.sortWealth();
    }
}
