
package bh0428_07;

public enum Currency {
    EUR(330), GBP(350), HUF(1), USD(400), JPN(200), RMB(150);
    
    private final int valueInHuf;

    private Currency(int valueInHuf) {
        this.valueInHuf = valueInHuf;
    }

    public int getValueInHuf() {
        return valueInHuf;
    }
}
