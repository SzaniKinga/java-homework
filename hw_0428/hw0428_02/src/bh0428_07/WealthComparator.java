/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0428_07;

import java.util.Comparator;

public class WealthComparator implements Comparator<Person>{

    @Override
    public int compare(Person o1, Person o2) {
        if (o1.getWealth() == o2.getWealth()) {
            return 0;
        }
        
        return o1.getWealth() > o2.getWealth() ? 1 : -1;
    }

}
