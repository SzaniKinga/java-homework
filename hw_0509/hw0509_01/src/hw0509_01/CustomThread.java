/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw0509_01;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Queue;

public class CustomThread extends Thread {

    Queue<String> lines;

    public CustomThread(Queue<String> lines) {
        this.lines = lines;
    }

    @Override
    public void run() {
        File f = new File("myfile.txt");
        StringBuilder sb;
        try (FileWriter fw = new FileWriter(f)) {
            while (!Thread.currentThread().isInterrupted()) {
                Iterator<String> value = lines.iterator();
                sb = new StringBuilder();
                while (value.hasNext()) {
                    sb.append(value.next());
                    sb.append("\n");
                    value.remove();
                }
                fw.write(sb.toString());
            }
        } catch (IOException e) {
            System.out.println("Fájlhiba történt");
        }
    }

}
