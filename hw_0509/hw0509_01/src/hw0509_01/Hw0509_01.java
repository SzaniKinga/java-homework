/*Írjon programot, ami egy szálon String sorokat kér be (ez akár lehet a „main” szál is) 
és beteszi az adatot egy Queue-ba, és egy másik szálon pedig ezt a Queue-t felhasználva 
kiírja egy fájlba. Ne feledkezzünk a synchronized-ról!
Addig olvassunk be a konzolról, amíg üres sort nem ütünk be.*/
package hw0509_01;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class Hw0509_01 {

    public static final Queue<String> lines = new PriorityQueue<>();

    public static void main(String[] args) throws InterruptedException {

        CustomThread ct = new CustomThread(lines);
        Scanner sc = new Scanner(System.in);
        System.out.println("Adja meg a sorokat!");
        String line;
        ct.start();
        do {
            line = sc.nextLine();
            synchronized (lines) {
                lines.add(line);
            }
        } while (!line.equals(""));
        ct.interrupt();

        
    }

}
