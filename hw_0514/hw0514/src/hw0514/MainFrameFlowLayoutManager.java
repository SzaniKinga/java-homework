/*Manuálisan hozzunk létre egy fájlt, amibe valamilyen szöveget beírunk. 
Olvassuk be ezt a fájlt, és minden sort tegyünk be egy label-be, és jelenítsük meg. 
Használjunk egy Layout Manager-t. JFileChooser*/
package hw0514;

import java.awt.FlowLayout;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MainFrameFlowLayoutManager extends JFrame {

    private JLabel label;

    public MainFrameFlowLayoutManager(String title) {
        super(title);
        this.label = new JLabel();
    }

    public void init(List<String> textData) {
        setVisible(true); 
        setSize(400, 200); 
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(new FlowLayout(5, 50, 50));
        
        for(String d: textData){
            label = new JLabel(d);
            add(label);
        }
       
    }

}
