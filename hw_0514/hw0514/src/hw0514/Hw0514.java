/*Manuálisan hozzunk létre egy fájlt, amibe valamilyen szöveget beírunk. 
Olvassuk be ezt a fájlt, és minden sort tegyünk be egy label-be, és jelenítsük meg. 
Használjunk egy Layout Manager-t. JFileChooser*/
package hw0514;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.swing.JLabel;

public class Hw0514 {

    public static void main(String[] args) {
        
        MainFrameFlowLayoutManager flow = new MainFrameFlowLayoutManager("MySwing");
        
        File f = new File("mytext.txt");
        List<String> textData = new ArrayList();
        
        try (BufferedReader br = new BufferedReader(new FileReader(f))) {
            String line = br.readLine();
            while (line != null) {
                textData.add(line);
                line = br.readLine();
            }
            flow.init(textData);
        } catch (Exception e) {
            System.out.println("Fájlhiba");
        }
 
    }
    
}
