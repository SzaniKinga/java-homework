/*Irjunk egy konyvolvaso Swinges programot ami kepes beolvasni egy szerializalt 
konyvet, vagy egy text file-t. Dontsuk el a kiterjesztes alapjan. .ser kiterjesztesu 
fileokat kezeljunk binaris szerializalt filenak .txt kiterjesztesueket pedig sima 
szovegfilenak. A file tartalmat toltsuk be egy textareaba. Egy save gomb lenyomasakor 
mentsuk le a textarea tartalmat egy szovegfileba. Egy serialize gomb megnyomasara 
szerializaljuk ki a textarea tartalmat egy fileba.*/
package bh0523_06;

public class App {

    public static void main(String[] args) {
        
        MainFrame frame = new MainFrame("Swing");
        frame.init();
    }
       
    
}
