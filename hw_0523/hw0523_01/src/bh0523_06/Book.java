/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh0523_06;

import java.io.Serializable;

public class Book implements Serializable {
    private String content;

    public Book(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return content;
    }
    
}
