/*Irjunk egy konyvolvaso Swinges programot ami kepes beolvasni egy szerializalt 
konyvet, vagy egy text file-t. Dontsuk el a kiterjesztes alapjan. .ser kiterjesztesu 
fileokat kezeljunk binaris szerializalt filenak .txt kiterjesztesueket pedig sima 
szovegfilenak. A file tartalmat toltsuk be egy textareaba. Egy save gomb lenyomasakor 
mentsuk le a textarea tartalmat egy szovegfileba. Egy serialize gomb megnyomasara 
szerializaljuk ki a textarea tartalmat egy fileba.*/
package bh0523_06;

import java.awt.FlowLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class MainFrame extends JFrame {

    private final JLabel label;
    private final JTextArea textArea;
    private final JButton readButton;
    private final JButton saveButton;
    private final JButton serializeButton;

    private static final String TEXT_FILE_NAME = "book.txt";
    private static final String SERIALIZED_FILE_NAME = "book.ser";

    public MainFrame(String title) {
        super(title);
        label = new JLabel("Book reader");
        textArea = new JTextArea(20, 30);
        readButton = new JButton("Read");
        saveButton = new JButton("Save");
        serializeButton = new JButton("Serialize");
    }

    public void init() {
        setSize(400, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.CENTER));
        add(label);
        add(textArea);
        add(readButton);
        add(saveButton);
        add(serializeButton);

        readButton.addActionListener(event -> {
            try {
                textArea.setText(read());
            } catch (IOException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        saveButton.addActionListener(event -> writeText());
        serializeButton.addActionListener(event -> writeSerialized());

        setVisible(true);
    }

    private void serialize() {
        try (FileOutputStream fos = new FileOutputStream(SERIALIZED_FILE_NAME);
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            Book book = new Book(textArea.getText());
            oos.writeObject(book);
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String read() throws IOException, FileNotFoundException, ClassNotFoundException {
        if (serializedFileExists()) {
            return readSerializedFile();
        } else if (textFileExists()) {
            return readTextFile();
        }

        throw new FileNotFoundException();
    }

    private String readSerializedFile() throws FileNotFoundException, IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(SERIALIZED_FILE_NAME);
        ObjectInputStream ois = new ObjectInputStream(fis);

        String contents = String.valueOf(ois.readObject());
        ois.close();

        return contents;
    }

    private String readTextFile() throws FileNotFoundException, IOException {
        File f = new File(TEXT_FILE_NAME);
        StringBuilder sb = new StringBuilder();

        try (FileReader fr = new FileReader(f);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
        } catch (IOException e) {
        }

        return sb.toString();
    }

    private boolean textFileExists() {
        return fileExists(TEXT_FILE_NAME);
    }

    private boolean serializedFileExists() {
        return fileExists(SERIALIZED_FILE_NAME);
    }

    private boolean fileExists(String fileName) {
        File f = new File(fileName);
        return f.exists();
    }

    private void writeText() {
        String text = textArea.getText();
        File f = new File(TEXT_FILE_NAME);
        try (FileWriter writer = new FileWriter(f)) {
            writer.write(text);
        } catch (IOException e) {
            System.out.println("A fájl nem található!");
        }
    }

    private void writeSerialized() {
        try (FileOutputStream fos = new FileOutputStream(SERIALIZED_FILE_NAME);
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(textArea.getText());
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
