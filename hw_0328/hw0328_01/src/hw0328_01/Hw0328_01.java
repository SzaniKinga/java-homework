/* 1. futóverseny eredményeinek feldolgozása: kérjük be a felhasználótól hány 
futó volt (legalább 3 ember kell a versenyhez), majd kérjük be a futók nevét 
és hogy hány perc alatt teljesítették a távot, majd a végén írjuk ki az első 
három helyezett futót és eredményeit. */
package hw0328_01;

import java.util.Scanner;

public class Hw0328_01 {

    public static void main(String[] args) {

        int runnersNumber = getNumberOfRunners();
        
        getNameOfRunners(runnersNumber);
        
        int[] runnersTime = getTimeOfRunners(runnersNumber);

        findWinners(runnersTime);
    }

    public static void findWinners(int[] array) {
        int firstPlace = Integer.MAX_VALUE;
        int secondPlace = Integer.MAX_VALUE;
        int thirdPlace = Integer.MAX_VALUE;
        int firstPlaceIndex = 0;
        int secondPlaceIndex = 0;
        int thirdPlaceIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < firstPlace) {
                thirdPlace = secondPlace;
                thirdPlaceIndex = secondPlaceIndex;
                secondPlace = firstPlace;
                secondPlaceIndex = firstPlaceIndex;
                firstPlace = array[i];
                firstPlaceIndex = i;
            } else if (array[i] < secondPlace) {
                thirdPlace = secondPlace;
                thirdPlaceIndex = secondPlaceIndex;
                secondPlace = array[i];
                secondPlaceIndex = i;
            } else if (array[i] < thirdPlace) {
                thirdPlace = array[i];
                thirdPlaceIndex = i;
            }
        }
        System.out.println("Az első helyezett sorszáma: " + (firstPlaceIndex + 1) + "., ideje: " + firstPlace + " perc");
        System.out.println("A második helyezett sorszáma: " + (secondPlaceIndex + 1) + "., ideje: " + secondPlace + " perc");
        System.out.println("A harmadik helyezett sorszáma: " + (thirdPlaceIndex + 1) + "., ideje: " + thirdPlace + " perc");
    }

    public static int[] getTimeOfRunners(int runnersNumber) {
        Scanner sc = new Scanner(System.in);
        int[] runnersTime = new int[runnersNumber];
        for (int i = 0; i < runnersTime.length; i++) {
            System.out.println("Kérem, adja meg a(z) " + (i + 1) + ". futó idejét percben: ");
            runnersTime[i] = sc.nextInt();
        }
        return runnersTime;
    }

    public static String[] getNameOfRunners(int runnersNumber) {
        Scanner sc = new Scanner(System.in);
        String[] runnersName = new String[runnersNumber];
        for (int i = 0; i < runnersName.length; i++) {
            System.out.println("Kérem, adja meg a(z) " + (i + 1) + ". futó nevét: ");
            runnersName[i] = sc.next();
        }
        return runnersName;
    }

    public static int getNumberOfRunners() {
        Scanner sc = new Scanner(System.in);
        int runnersNumber;
        do {
            System.out.println("Kérem, adja meg a futók számát (min. 3 fő): ");
            runnersNumber = sc.nextInt();
        } while (runnersNumber < 3);
        return runnersNumber;
    }

}
