/*2. generáljunk egy tömböt véletlen számokkal, majd írjuk ki azt a számot, 
amikortól kezdve tőle csak nagyobb szám van a tömbben. 
pl. 3, 4, 7, 3, 4, 6 esetén a 3 a megoldás
9, 3, 1, 5, 2, 4, 6 esetén a 2 a megoldás */
package hw_0328_02;

public class Hw_0328_02 {

    public static void main(String[] args) {

        int[] array = fillRandomArray(8, 1, 6);
        printItems(array);

        System.out.println("Ettől a számtól jobbra csak nagyobbak találhatók a tömbben:");
        findMinItem(array);

    }

    public static void findMinItem(int[] arr) {
        int item = arr[arr.length - 1];
        for (int i = arr.length - 2; i > 0; i--) {
            if (item < arr[i]) {
                item = arr[arr.length - 1];
                break;
            } else if (item >= arr[i] && arr[i] < arr[i - 1]) {
                item = arr[i];
                break;
            } else if (item >= arr[i] && arr[i] >= arr[i - 1]) {
                item = arr[i - 1];
            }
        }
        System.out.println(item);
    }

    public static void printItems(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
    }

    public static int[] fillRandomArray(int size, int min, int max) {
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = (int) (Math.random() * (max - min + 1) + min);
        }
        return arr;
    }

}
