package hu.braininghub.bookwebhw.model;

import hu.braininghub.bookwebhw.entities.Author;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class AuthorDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    public void addNewAuthor(AuthorDTO dto) {
        Author a = AuthorMapper.toEntity(dto);
        em.persist(a); 
    }
    
    public void removeAuthor(int id) {
        Author a = this.getAuthorById(id);
        em.remove(a);
    }

    public Author getAuthorById(int id) {
        Author a = em.find(Author.class, id); 
        return a;
    }

    public List<AuthorDTO> getAuthors() { 
        List<Author> authors = em.createQuery("SELECT a FROM Author a").getResultList(); 
        List<AuthorDTO> authorDTOs = new ArrayList();
        
        for(Author author: authors){
            authorDTOs.add(AuthorMapper.toDTO(author)); 
        }
        
        return authorDTOs;
    }

    
    
}
