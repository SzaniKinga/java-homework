/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bookwebhw.service;

import hu.braininghub.bookwebhw.entities.Book;
import hu.braininghub.bookwebhw.model.AuthorDTO;
import hu.braininghub.bookwebhw.model.BookDAO;
import hu.braininghub.bookwebhw.model.BookDTO;
import java.sql.SQLException;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class BookService {

    @Inject 
    BookDAO dao; 
    
    public void addBook(String title, String description, List<AuthorDTO> authorDTOs) {
        BookDTO dto = new BookDTO();
        dto.setTitle(title);
        dto.setDescription(description);
        dto.setAuthorDTOs(authorDTOs);
        
        dao.addNewBook(dto);
    }

    public void removeBook(int bookId) {
        dao.removeBook(bookId);
    }

    public List<BookDTO> getBooks() throws SQLException {
        return dao.getBooks();
    }
    
    public List<BookDTO> getBooks(String search) throws SQLException {
        return dao.getBooks(search);
    }

    public Book getBookById(int id) {
        return dao.getBookById(id);
    }
}
