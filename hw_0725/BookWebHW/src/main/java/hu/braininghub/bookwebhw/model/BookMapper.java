/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bookwebhw.model;

import hu.braininghub.bookwebhw.entities.Author;
import hu.braininghub.bookwebhw.entities.Book;
import java.util.ArrayList;
import java.util.List;

public class BookMapper {

    public static BookDTO toDTO(Book book) {
        BookDTO dto = new BookDTO();
        
        dto.setId(book.getId());
        dto.setTitle(book.getTitle());
        dto.setDescription(book.getDescription());
        
        dto.setAuthorDTOs(toAuthorDTOs(book.getAuthors()));
        return dto;
    }
    
    public static Book toEntity(BookDTO dto) {
        Book book = new Book();
        
        book.setId(dto.getId());
        book.setTitle(dto.getTitle());
        book.setDescription(dto.getDescription());
        book.setAuthors(toAuthorEntities(dto.getAuthorDTOs()));

        return book;
    }
    
    private static List<AuthorDTO> toAuthorDTOs(List<Author> authors) {
        List<AuthorDTO> authorDTOs = new ArrayList<>();
        for (Author author: authors) {
            authorDTOs.add(AuthorMapper.toDTO(author));
        }
        
        return authorDTOs;
    }
    
     private static List<Author> toAuthorEntities(List<AuthorDTO> authorDTOs) {
        List<Author> authors = new ArrayList<>();
        for (AuthorDTO authorDTO: authorDTOs) {
            authors.add(AuthorMapper.toEntity(authorDTO));
        }
        
        return authors;
    }
}
