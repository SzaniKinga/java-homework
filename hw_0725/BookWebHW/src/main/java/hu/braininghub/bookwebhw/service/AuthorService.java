/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bookwebhw.service;

import hu.braininghub.bookwebhw.entities.Author;
import hu.braininghub.bookwebhw.model.AuthorDAO;
import hu.braininghub.bookwebhw.model.AuthorDTO;
import hu.braininghub.bookwebhw.model.BookDTO;
import java.sql.SQLException;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class AuthorService {

    @Inject 
    AuthorDAO dao; 
    
    public void addAuthor(String name, List<BookDTO> bookDTOs) {
        AuthorDTO dto = new AuthorDTO();
        dto.setName(name);
        dto.setBookDTOs(bookDTOs);
        
        dao.addNewAuthor(dto);
    }

    public void removeAuthor(int authorId) {
        dao.removeAuthor(authorId);
    }

    public List<AuthorDTO> getAuthors() throws SQLException {
        return dao.getAuthors();
    }

    public Author getAuthorById(int id) {
        return dao.getAuthorById(id);
    }
}
