package hu.braininghub.bookwebhw.model;

import hu.braininghub.bookwebhw.entities.Book;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Singleton
public class BookDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    public void addNewBook(BookDTO dto) {
        Book b = BookMapper.toEntity(dto);
        em.persist(b); 
    }
    
    public void removeBook(int id) {
        Book b = this.getBookById(id);
        em.remove(b);
    }

    public Book getBookById(int id) {
        Book b = em.find(Book.class, id); 
        return b;
    }

    public List<BookDTO> getBooks() { 
        List<Book> books = em.createQuery("SELECT b FROM Book b").getResultList();
        List<BookDTO> bookDTOs = new ArrayList();
        
        for(Book book: books){
            bookDTOs.add(BookMapper.toDTO(book)); 
        }
        
        return bookDTOs;
    }
    
    public List<BookDTO> getBooks(String search) { 
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery q = cb.createQuery(Book.class);
        Root<Book> book = q.from(Book.class);
        q.select(book);
        
        Expression<String> title = book.get("title");
        Expression<String> description = book.get("description");
        Predicate p1 = cb.like(title, "%" + search + "%");
        Predicate p2 = cb.like(description, "%" + search + "%");
        Predicate finalPredicate = cb.or(p1, p2);
        q.where(finalPredicate);
   
        List<Book> books = em.createQuery(q).getResultList(); 
        List<BookDTO> bookDTOs = new ArrayList();
        
        for(Book b: books){
            bookDTOs.add(BookMapper.toDTO(b)); 
        }
        
        return bookDTOs;
    }

    
    
}
