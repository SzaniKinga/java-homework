/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bookwebhw.model;

import java.util.List;
import java.util.Objects;

public class BookDTO {
    
    private long id;
    private String title;
    private String description;
    private List<AuthorDTO> authorDTOs;

    public BookDTO(long id, String title, String description, List<AuthorDTO> authorDTOs) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.authorDTOs = authorDTOs;
    }
    
    public BookDTO() {
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AuthorDTO> getAuthorDTOs() {
        return authorDTOs;
    }

    public void setAuthorDTOs(List<AuthorDTO> authorDTOs) {
        this.authorDTOs = authorDTOs;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 11 * hash + Objects.hashCode(this.title);
        hash = 11 * hash + Objects.hashCode(this.description);
        hash = 11 * hash + Objects.hashCode(this.authorDTOs);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BookDTO other = (BookDTO) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.authorDTOs, other.authorDTOs)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "BookDTO{" + "id=" + id + ", title=" + title + ", description=" + description + ", authorDTOs=" + authorDTOs + '}';
    }

   
    
}
