/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bookwebhw.model;

import java.util.List;
import java.util.Objects;

public class AuthorDTO {
    
    private long id;
    private String name;
    private List<BookDTO> bookDTOs;

    public AuthorDTO(long id, String name, List<BookDTO> bookDTOs) {
        this.id = id;
        this.name = name;
        this.bookDTOs = bookDTOs;
    }

    public AuthorDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BookDTO> getBookDTOs() {
        return bookDTOs;
    }

    public void setBookDTOs(List<BookDTO> bookDTOs) {
        this.bookDTOs = bookDTOs;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.bookDTOs);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AuthorDTO other = (AuthorDTO) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.bookDTOs, other.bookDTOs)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AuthorDTO{" + "id=" + id + ", name=" + name + ", bookDTOs=" + bookDTOs + '}';
    }
    
}
