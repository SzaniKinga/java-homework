/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bookwebhw.model;

import hu.braininghub.bookwebhw.entities.Author;

public class AuthorMapper {

    public static AuthorDTO toDTO(Author author) {
        AuthorDTO dto = new AuthorDTO();
        
        dto.setId(author.getId());
        dto.setName(author.getName());

        return dto;
    }
    
    public static Author toEntity(AuthorDTO dto) {
        Author author = new Author();
        
        author.setId(dto.getId());
        author.setName(dto.getName());

        return author;
    }
}
