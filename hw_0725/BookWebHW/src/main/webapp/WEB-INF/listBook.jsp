

<%@page import="hu.braininghub.bookwebhw.entities.model.BookDTO"%>
<%@page import="java.util.List"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Könyvek</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>        

    </head>
    <body>
        <form action="BookServlet" method="post">
            <div class="form-group">
                <label for="search">Search</label>
                 <input type="text" class="form-control" id="search" placeholder="Search" name="search">
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
        <% List<BookDTO> bookDTOs = (List<BookDTO>) request.getAttribute("bookDTOs");%>
        <div class="container">
            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author(s)</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${bookDTOs}" var="book">
                            <tr>
                                <td>${book.title}</td>
                                <td>
                                    <ul>
                                        <c:forEach items="${book.authorDTOs}" var="author">
                                            <li>${author.name}</li>
                                        </c:forEach>
                                    </ul>
                                </td>
                                <td>${book.description}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
    </body>
</html>

