insert into book(id, title, description) values (-10, 'Harry Potter és a bölcsek köve', 'A sorozat első kötete');
insert into book(id, title, description) values (-11, 'Java programozás', 'Jó hosszú könyv a Java-ról');
insert into book(id, title, description) values (-12, 'Design Patterns', 'Játékos módon sajátítjuk el a legfontosabb tervezési mintákat');
insert into book(id, title, description) values (-13, 'Éld jól az életed', 'A könyvből megtanulhatjuk, hogyan tehetjük teljesebbé az életünket');

insert into author(id, name) values (-10, 'J. K. Rowling');
insert into author(id, name) values (-11, 'Nagy Gusztáv');
insert into author(id, name) values (-12, 'Kathy Sierra ');
insert into author(id, name) values (-13, 'Louise L. Hay');

insert into book_author_jt(book_id, author_id) values (-10, -10);
insert into book_author_jt(book_id, author_id) values (-10, -11);
insert into book_author_jt(book_id, author_id) values (-11, -12);
insert into book_author_jt(book_id, author_id) values (-12, -12);
insert into book_author_jt(book_id, author_id) values (-12, -13);
insert into book_author_jt(book_id, author_id) values (-13, -10);
insert into book_author_jt(book_id, author_id) values (-13, -13);