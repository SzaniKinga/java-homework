package hu.braininghub.bookwebhw.entities;

import hu.braininghub.bookwebhw.entities.Book;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-28T16:29:52")
@StaticMetamodel(Author.class)
public class Author_ { 

    public static volatile ListAttribute<Author, Book> books;
    public static volatile SingularAttribute<Author, String> name;
    public static volatile SingularAttribute<Author, Long> id;

}