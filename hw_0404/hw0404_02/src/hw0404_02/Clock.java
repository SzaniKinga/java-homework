/*Csináljatok egy órát megvalósító osztályt, az órának három adattagja legyen:
- óra
- perc
- másodperc
Tudja kiírni hogy mennyi az idő (toString), lehessen hozzáadni még perceket 
(plusMinute(int minute) metódus), ami változtatja a percek számát.*/
package hw0404_02;

public class Clock {

    private int hours;
    private int minutes;
    private int seconds;
    
    public Clock(int hours, int minutes, int seconds){
        if (hours > 23) {
            hours = 23;
        } else if (hours < 0) {
            hours = 0;
        }
        if (minutes > 59) {
            minutes = 59;
        } else if (minutes < 0) {
            minutes = 0;
        }
        if (seconds > 59) {
            seconds = 59;
        } else if (seconds < 0) {
            seconds = 0;
        } 
        
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }
    
    public void plusMinute(int minute) {
        int addedHours = minute / 60;
        minute -= addedHours * 60;
        
        this.minutes += minute;
        int hoursOfIncreasedMinutes = this.minutes / 60;
        this.minutes -= hoursOfIncreasedMinutes * 60;
        
        this.hours += addedHours + hoursOfIncreasedMinutes;
        
        if (this.hours > 23) {
            this.hours -= 24;
        }
    }
    
    @Override
    public String toString() {
        String hourFiller = hours < 10 ? "0" : ""; 
        String minuteFiller = minutes < 10 ? "0" : ""; 
        String secondFiller = seconds < 10 ? "0" : ""; 
        return hourFiller + hours + ":" + minuteFiller + minutes + ":" + secondFiller + seconds;
    }

}
