/*Csináljatok egy órát megvalósító osztályt, az órának három adattagja legyen:
- óra
- perc
- másodperc
Tudja kiírni hogy mennyi az idő (toString), lehessen hozzáadni még perceket 
(plusMinute(int minute) metódus), ami változtatja a percek számát.*/
package hw0404_02;

import java.util.Scanner;

public class Hw0404_02 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Kérem, adja meg az órát");
        int hour = sc.nextInt();
        System.out.println("Kérem, adja meg a perceket");
        int minute = sc.nextInt();
        System.out.println("Kérem, adja meg a másodperceket");
        int second = sc.nextInt();

        Clock clock = new Clock(hour, minute, second);
        System.out.println(clock);

        do {
            System.out.println("Kérem, adja meg hány percet adjunk hozzá (-1 a kilépés)");
            minute = sc.nextInt();
            if (minute >= 0) {
                clock.plusMinute(minute);
                System.out.println(clock);
            }
        } while (minute != -1);
    }

}
