package bh0404_05;

import java.util.Scanner; 

public class SmartCalculator {

    public static Scanner sc = new Scanner(System.in);

    private int number;
    private char operation;
    private double result;
    private boolean firstOperation = true;

    public void askNumber() {
        System.out.println("Kérem, adjon meg egy számot: ");
        this.number = sc.nextInt();
        handleEqualOperation();
        askOperation();
    }

    public void askOperation() {
        System.out.println("Kérem, adjon meg egy műveletet (+,-,=): ");
        char op = sc.next().charAt(0);

        switch (op) {
            case '=':
                printResult();
                break;
            default:
                this.operation = op;
                askNumber();
                break;
        }
    }

    public void handleEqualOperation() {
        if (firstOperation) {
            result = number;
            firstOperation = false;
        } else {
            switch (this.operation) {
                case '+':
                    result += number;
                    break;
                case '-':
                    result -= number;
                    break;
            }
        }
    }

    public void start() {
        askNumber();
    }

    public void printResult() {
        System.out.println("result: " + this.result);
    }

}
