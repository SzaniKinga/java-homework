
package cars;

import cars.Car;

public class ElectricCar extends Car {
    
    private double chargeLevel;

    public ElectricCar(String brand, String model, String color, int year, int kmCount, String plateNumber, String VINnumber, int price, int doorNumber, int weight, double chargeLevel) {
        super(brand, model, color, year, kmCount, plateNumber, VINnumber, price, doorNumber, weight);
        this.chargeLevel = chargeLevel;
    }

    @Override
    public double getCapacityInKm() {
        return this.chargeLevel*this.efficiencyRate()*350;
    }
    
    @Override
    protected double efficiencyRate() {
        return Math.max(0.6, 1-getKmCount()/280000.0);
    }

}
