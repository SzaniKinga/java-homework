package cars;

import cars.Car;
import cars.DieselCar;
import cars.ElectricCar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CarRandomGenerator {

    private static Random rnd = new Random();

    public Car getRandomCar(int count) {

        int rndNumber = rnd.nextInt(100);
        String brand = getRandomBrand();

        if (rndNumber < 33) {
            return new ElectricCar(brand, getRandomModel(brand), getRandomColor(), getRandomYear(),
                    getRandomKmCount(), getRandomPlateNumber(), getRandomVINnumber(), getRandomPrice(),
                    getRandomDoorNumber(), getRandomWeight(), getRandomChargeLevel());
        }
        if (rndNumber < 66) {
            return new PetrolCar(brand, getRandomModel(brand), getRandomColor(), getRandomYear(),
                    getRandomKmCount(), getRandomPlateNumber(), getRandomVINnumber(), getRandomPrice(),
                    getRandomDoorNumber(), getRandomWeight(), getRandomFuel());
        }
        return new DieselCar(brand, getRandomModel(brand), getRandomColor(), getRandomYear(),
                getRandomKmCount(), getRandomPlateNumber(), getRandomVINnumber(), getRandomPrice(),
                getRandomDoorNumber(), getRandomWeight(), getRandomFuel());
    }

    public String getRandomBrand() {
        String[] brands = {"BMW", "Opel", "Suzuki", "Ferrari"};
        int random = rnd.nextInt(4);
        return brands[random];
    }

    public String getRandomModel(String brand) {
        String[] bmwModels = {"X4", "X5", "X6", "X7"};
        String[] opelModels = {"Astra", "Vectra", "Corsa", "Insignia"};
        String[] suzukiModels = {"Swift", "Wagon R", "Swift+", "Vitara"};
        String[] ferrariModels = {"458 Spider", "355 F1", "458 Speciale", "208 GT4"};
        int random = rnd.nextInt(4);

        switch (brand) {
            case "BMW":
                return bmwModels[random];
            case "Opel":
                return opelModels[random];
            case "Suzuki":
                return suzukiModels[random];
            case "Ferrari":
                return ferrariModels[random];
            default:
                return "";
        }
    }

    public String getRandomColor() {
        String[] colors = {"white", "silver", "black", "blue"};
        int random = rnd.nextInt(4);
        return colors[random];
    }

    public int getRandomYear() {
        return rnd.nextInt(15) + 2005;
    }

    public int getRandomKmCount() {
        return rnd.nextInt(100000) + 60000;
    }

    public String getRandomPlateNumber() {
        String plateNumber = "";
        for (int i = 0; i < 3; i++) {
            plateNumber += getRandomChar();
        }
        plateNumber += "-";
        for (int i = 0; i < 3; i++) {
            plateNumber += getRandomInt();
        }
        return plateNumber;
    }

    public char getRandomChar() {
        return (char) (rnd.nextInt(26) + 65);
    }

    public int getRandomInt() {
        return rnd.nextInt(10);
    }

    public String getRandomVINnumber() {
        String VINnumber = "";
        for (int i = 0; i < 17; i++) {
            int random = rnd.nextInt(2);
            if (random == 0) {
                VINnumber += getRandomInt();
            } else {
                VINnumber += getRandomChar();
            }
        }
        return VINnumber;
    }

    public int getRandomPrice() {
        return rnd.nextInt(10000) + 10000;
    }

    public int getRandomDoorNumber() {
        return rnd.nextInt(4) + 2;
    }

    public int getRandomWeight() {
        return rnd.nextInt(300) + 1000;
    }

    public double getRandomChargeLevel() {
        return (double) rnd.nextInt(51) + 50;
    }

    private int getRandomFuel() {
        return rnd.nextInt(21) + 40;
    }
}
