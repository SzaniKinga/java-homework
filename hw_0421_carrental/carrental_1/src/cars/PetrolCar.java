
package cars;

import cars.Car;

public class PetrolCar extends Car {
    
    private int fuel;

    public PetrolCar(String brand, String model, String color, int year, int kmCount, String plateNumber, String VINnumber, int price, int doorNumber, int weight, int fuel) {
        super(brand, model, color, year, kmCount, plateNumber, VINnumber, price, doorNumber, weight);
        this.fuel = fuel;
    }

    @Override
    public double getCapacityInKm() {
        return this.fuel*10*this.efficiencyRate();
    }

    @Override
    protected double efficiencyRate() {
        return Math.max(0.6, 1-getKmCount()/240000);
    }

}
