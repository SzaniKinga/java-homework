package cars;

import cars.Car;

public class DieselCar extends Car {

    private int fuel;

    public DieselCar(String brand, String model, String color, int year, int kmCount, String plateNumber, String VINnumber, int price, int doorNumber, int weight, int fuel) {
        super(brand, model, color, year, kmCount, plateNumber, VINnumber, price, doorNumber, weight);
        this.fuel = fuel;
    }

    @Override
    public double getCapacityInKm() {
        return this.fuel*13*this.efficiencyRate();
    }

    @Override
    protected double efficiencyRate() {
        return Math.max(0.7, 1-getKmCount()/520000);
    }
    
    

}
