package cars;

import java.util.Objects;

public abstract class Car {

    private String brand;
    private String model;
    private String color;
    private int year;
    private int kmCount;
    private String plateNumber;
    private String VINnumber;
    private int price;
    private int doorNumber;
    private int weight;

    public Car(String brand, String model, String color, int year, int kmCount, String plateNumber, String VINnumber, int price, int doorNumber, int weight) {
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.year = year;
        this.kmCount = kmCount;
        this.plateNumber = plateNumber;
        this.VINnumber = VINnumber;
        this.price = price;
        this.doorNumber = doorNumber;
        this.weight = weight;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getKmCount() {
        return kmCount;
    }

    public void setKmCount(int kmCount) {
        this.kmCount = kmCount;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getVINnumber() {
        return VINnumber;
    }

    public void setVINnumber(String VINnumber) {
        this.VINnumber = VINnumber;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDoorNumber() {
        return doorNumber;
    }

    public void setDoorNumber(int doorNumber) {
        this.doorNumber = doorNumber;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.plateNumber);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Car other = (Car) obj;
        if (!Objects.equals(this.plateNumber, other.plateNumber)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return color + " " + brand + " " + model + " (" + year + ") -  " + price + " Ft";
    }

    public abstract double getCapacityInKm();

    protected abstract double efficiencyRate();

}
