package shop;

import cars.Car;
import java.util.ArrayList;
import java.util.List;

public class Site {

    private String name;

    private String address;

    private List<Car> cars = new ArrayList<>();

    public Site(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Car> getCarsExcept(List<Car> exceptions) {
        List<Car> remainingCars = new ArrayList<>();
        boolean available;
        for (Car car : cars) {
            available = true;
            for (Car exception : exceptions) {
                if (car.equals(exception)) {
                    available = false;
                    break;
                }
            }
            if (available) {
                remainingCars.add(car);
            }
        }
        return remainingCars;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void addCar(Car c) {
        this.cars.add(c);
    }

    @Override
    public String toString() {
        return "Site{" + "name=" + name + ", address=" + address + '}';
    }

    public Car getCarByPlateNumber(String plateNumber) {
        for (Car car : cars) {
            if (car.getPlateNumber().equals(plateNumber)) {
                return car;
            }
        }

        return null;
    }
    
     public Car getCarByPlateNumber(String plateNumber, List<Car> rented) {
        for (Car car : getCarsExcept(rented)) {
            if (car.getPlateNumber().equals(plateNumber)) {
                return car;
            }
        }

        return null;
    }

    public void listCars() {
        for (Car c : this.cars) {
            printCar(c);
        }
    }

    public void listCars(int minRange) {
        for (Car c : this.cars) {
            if (c.getCapacityInKm() >= minRange) {
                printCar(c);
            }
        }
    }
    
    public void listCars(int minRange, List<Car> exceptions) {
        for (Car c : getCarsExcept(exceptions)) {
            if (c.getCapacityInKm() >= minRange) {
                printCar(c);
            }
        }
    }

    private void printCar(Car c) {
        System.out.println(c.getPlateNumber() + " " + c.getColor() + " " + c.getBrand()
                + " " + c.getModel() + " " + c.getYear());
    }

}
