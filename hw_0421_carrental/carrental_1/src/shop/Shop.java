/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop;

import cars.Car;
import cars.CarRandomGenerator;
import customer.Customer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Shop {

    private static Random rnd = new Random();
    public List<Site> sites = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);

    public Shop() {
        initRandomSites();
    }

    public void initRandomSites() {
        Site nyiregyhaza = new Site("TT", "Nyíregyháza");
        Site debrecen = new Site("TD", "Debrecen");
        int count = 0;
        CarRandomGenerator random = new CarRandomGenerator();
        for (int i = 0; i < rnd.nextInt(50); i++) {
            nyiregyhaza.addCar(random.getRandomCar(count++));
        }
        for (int i = 0; i < rnd.nextInt(100); i++) {
            debrecen.addCar(random.getRandomCar(count++));
        }
        sites.add(debrecen);
        sites.add(nyiregyhaza);

    }

    public void chooseMenu(Customer customer) {

        while (true) {
            System.out.println("Kérem válasszon as alábbi lehetőségek közül");
            System.out.println("\t1. telephelyek listázása");
            System.out.println("\t2. adott telephelyen lévő kocsik");
            System.out.println("\t3. adott telephelyen lévő kocsik, amelyek több mint 150 km-t tudnak megtenni");
            System.out.println("\t4. cég autóinak kölcsönzési árai");
            System.out.println("\t5. kölcsönzés");
            System.out.println("\t6. kilépés a menüből");

            Site s;
            int number = scanner.nextInt();
            switch (number) {
                case 1:
                    listSites();
                    break;
                case 2:
                    s = chooseSite();
                    showCarsInSite(s);
                    break;
                case 3:
                    s = chooseSite();
                    showCarsInSite(s, 150);
                    break;
                case 4:
                    for (Site site : this.sites) {
                        System.out.println(site.getName() + ":");
                        for (Car car : site.getCars()) {
                            System.out.println(car);
                        }
                        System.out.println("-------------------");
                    }
                    break;
                case 5:
                    rentCar(customer);
                    break;
                case 6:
                    System.exit(0);
                    break;    
            }
        }
    }

    private void listSites() {
        for (Site site : this.sites) {
            System.out.println(site.toString());
        }
    }

    private Site chooseSite() {
        System.out.println("Kérem, válasszon az alábbi telephelyek közül a neve megadásával");
        this.listSites();
        String siteName = scanner.next();
        for (Site s : this.sites) {
            if (siteName.equals(s.getName())) {
                return s;
            }
        }
        return null;
    }

    private void showCarsInSite(Site s) {
        s.listCars();
    }
    
    private void showCarsInSite(Site s, int minRange) {
        s.listCars(minRange);
    }
    
      private void showCarsInSite(Site s, int minRange, List<Car> rented) {
        s.listCars(minRange, rented);
    }

    private void rentCar(Customer customer) {
        Site s = chooseSite();
        Rental rental = new Rental();
        
        System.out.println("Hány km-t akar megtenni?");
        int minKm = scanner.nextInt();
        System.out.println("Hány autót akar bérelni?"); 
        int carCount = scanner.nextInt();
        
        for (int i = 0; i < carCount; i++) {
            showCarsInSite(s, minKm, rental.getCars());
            System.out.println("Kérem, adja meg a rendszámot");
            String plateNumber = scanner.next();
            Car car = s.getCarByPlateNumber(plateNumber, rental.getCars());
            if (car == null) {
                i--;
                System.out.println("Nincs ilyen autó!");
                continue;
            }
            rental.addCar(car);
        }
        
        rental.setCustomer(customer);
        rental.printRental();
    }

}
