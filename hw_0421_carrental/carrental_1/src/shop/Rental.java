/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop;

import cars.Car;
import customer.Customer;
import java.util.ArrayList;
import java.util.List;

public class Rental { 
    private static final int QUANTITY_DISCOUNT_THRESHOLD = 10;
    private static final int QUANTITY_DISCOUNT_MAX = 6;
    private static final int QUANTITY_DISCOUNT_VALUE = 5;
    
    private List<Car> cars = new ArrayList<>();
    private Customer customer;
    
    public List<Car> getCars(){
        return cars;
    }
    
    public void addCar(Car car) {
        cars.add(car);
    }
   
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    public int getTotal() {
        int total = 0;
        for (Car car: cars) {
            total += car.getPrice();
        }
        
        return total;
    }
    
    private double getCustomerDiscountAmount() {
        int total = getTotal();
        
        return total * customer.ownDiscount() / 100;
    }
    
    private double getQuantityDiscountAmount()
    {
        int total = getTotal();
        
        return total * getQuantityDiscountPercent() / 100;
    }
    
    private int getQuantityDiscountPercent() {
        int quantityDiscountBase = Math.min(cars.size() / QUANTITY_DISCOUNT_THRESHOLD, QUANTITY_DISCOUNT_MAX);
        
        return quantityDiscountBase * QUANTITY_DISCOUNT_VALUE;
    }
    
    public void printRental() {
        int total = getTotal();
        System.out.println("Bérelt autók:");
        for (Car car: cars) {
            System.out.println(car.getPlateNumber() + " " + car.getBrand() + " " + car.getModel()
            + " " + car.getYear() + ": " + car.getPrice() + " Ft");
        }
        
        System.out.println("Teljes ár: " + total);
        System.out.println("------------------");
        boolean discount = false;
        if (customer.ownDiscount() > 0) {
            discount = true;
            System.out.println("Törzsvásárlói kedvezmény: " + getCustomerDiscountAmount() + " Ft");
        }
        
        if (getQuantityDiscountPercent() > 0) {
            discount = true;
            System.out.println("Mennyiségi kedvezmény: " + getQuantityDiscountAmount() + " Ft");
        }
        
        if (discount) {
            System.out.println("Kedvezményes ár: " + 
                    (total - getCustomerDiscountAmount() - getQuantityDiscountAmount()) + " Ft");
        }
        System.out.println("Köszönjük, hogy nálunk bérelt");
        System.out.println("------------------");
    }
}
