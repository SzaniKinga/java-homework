/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customer;

import customer.Customer;

public class PersonalCustomer extends Customer {

    private boolean platina;
    
    public PersonalCustomer(String name) { //átmutat a másik konstruktorba
        this(name,false); //a sima this-szel konstruktort hív
    }
    
    public PersonalCustomer(String name, boolean platina) {
        super(name);
        this.platina = platina;
    }

    @Override
    public double ownDiscount() {
        if(platina){
            return 7.5;
        }else
            return 0;
    }

    public boolean isPlatina() {
        return platina;
    }

    public void setPlatina(boolean platina) {
        this.platina = platina;
    }
 
}
