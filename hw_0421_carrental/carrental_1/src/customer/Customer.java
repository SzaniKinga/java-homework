package customer;

import cars.Car;
import java.util.ArrayList;
import java.util.List;

public abstract class Customer {

    private String name;
    private List<Car> cars = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addCar(Car c) {
        this.cars.add(c);
    }

    public int getCarCount() {
        return this.cars.size();
    }

    public int getExtraDiscount() {
        return (getCarCount() / 10) * 5;
    }

    public double getDiscount() {
        double discount = ownDiscount() + getExtraDiscount();
        if (discount <= 30) {
            return discount;
        } else {
            return 30;
        }
    }

    public abstract double ownDiscount();

}
