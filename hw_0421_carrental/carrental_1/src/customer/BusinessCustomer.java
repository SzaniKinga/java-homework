/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customer;

import customer.Customer;

public class BusinessCustomer extends Customer {

    public BusinessCustomer(String name) {
        super(name);
    }

    @Override
    public double ownDiscount() {
        return 10;
    }
    
    
    
}
