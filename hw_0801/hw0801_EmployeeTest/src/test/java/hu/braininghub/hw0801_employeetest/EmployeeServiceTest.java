/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.hw0801_employeetest;

import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EmployeeServiceTest {

    EmployeeService s;

    public EmployeeServiceTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        s = new EmployeeService();
        s.resetEmployees();
    }

    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void getMaxSalaryEmpl_emptyList_returnsFalse() {
        
        Optional<Employee> maxEmployee = s.getMaxSalaryEmployee();
        
        Assertions.assertFalse(maxEmployee.isPresent(), "nem jó az eredmény");

    }

    @Test
    public void getMaxSalaryEmpl_salary300and500and400_returns500() {

        Employee e1 = new Employee("Józsi", 25, 300);
        Employee e2 = new Employee("Béla", 31, 500);
        Employee e3 = new Employee("Feri", 28, 400);

        s.addEmployee(e1);
        s.addEmployee(e2);
        s.addEmployee(e3);

        Employee maxEmployee = s.getMaxSalaryEmployee().get();

        Assertions.assertEquals(500, maxEmployee.getSalary(), "nem jó az eredmény");

    }
    
    @Test
    public void getAverageSalaryByAge_emptyList_returnsEmptyMap() {
        
        Map<Integer, Double> averageSalaries = s.getSalaryAverageByAge();

        Assertions.assertEquals(0, averageSalaries.size(), "nem jó az eredmény");

    }
    
    @Test
    public void getAverageSalaryByAge_salary300WithAge20_returns300() {

        Employee e1 = new Employee("Józsi", 20, 300);

        s.addEmployee(e1);

        Map<Integer, Double> averageSalaries = s.getSalaryAverageByAge();

        Assertions.assertEquals(300, averageSalaries.get(20), "nem jó az eredmény");

    }


    @Test
    public void getAverageSalaryByAge_salary300WithAge20AndSalary350WithAge22AndSalary400WithAge20_returns350() {

        Employee e1 = new Employee("Józsi", 20, 300);
        Employee e2 = new Employee("Béla", 22, 350);
        Employee e3 = new Employee("Feri", 20, 400);

        s.addEmployee(e1);
        s.addEmployee(e2);
        s.addEmployee(e3);

        Map<Integer, Double> averageSalaries = s.getSalaryAverageByAge();

        Assertions.assertEquals(350, averageSalaries.get(20), "nem jó az eredmény");

    }

    @Test
    public void getAverageSalaryByAge_salary300WithAge20AndSalary350WithAge22AndSalary400WithAge20AndSalary400WithAge22_returns375() {

        Employee e1 = new Employee("Józsi", 20, 300);
        Employee e2 = new Employee("Béla", 22, 350);
        Employee e3 = new Employee("Feri", 20, 400);
        Employee e4 = new Employee("Sanyi", 22, 400);

        s.addEmployee(e1);
        s.addEmployee(e2);
        s.addEmployee(e3);
        s.addEmployee(e4);

        Map<Integer, Double> averageSalaries = s.getSalaryAverageByAge();

        Assertions.assertEquals(375, averageSalaries.get(22), "nem jó az eredmény");

    }

}
