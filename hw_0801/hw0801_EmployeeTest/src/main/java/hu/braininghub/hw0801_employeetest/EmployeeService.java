/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.hw0801_employeetest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

public class EmployeeService {

    private List<Employee> employees;

    public EmployeeService() {
        employees = new ArrayList();

        for (int i = 0; i < 10; i++) {
            Employee e = createRandomEmployee();
            employees.add(e);
        }

        System.out.println(employees);
    }

    public void addEmployee(Employee e) {

        employees.add(e);
    }

    public void resetEmployees() {

        employees = new ArrayList();
    }

    private Employee createRandomEmployee() {

        String[] names = {"Béla", "Józsi", "Sanyi", "Irén", "Mari", "Erzsi"};

        Random r = new Random();
        String name = names[r.nextInt(names.length)];

        int age = r.nextInt(30) + 20;
        int salary = r.nextInt(400000) + 150000;

        Employee e = new Employee(name, age, salary);

        return e;
    }

    public Optional<Employee> getMaxSalaryEmployee() {

        EmployeeSalaryComparator c = new EmployeeSalaryComparator();
        return employees.stream().max(c);

    }

    public Map<Integer, Double> getSalaryAverageByAge() {

        Map<Integer, List<Integer>> employeeBySalaryMap = new HashMap<>();
        for (Employee e : employees) {
            if (employeeBySalaryMap.containsKey(e.getAge())) {
                employeeBySalaryMap.get(e.getAge()).add(e.getSalary());
            } else {
                List<Integer> list = new ArrayList<>();
                list.add(e.getSalary());
                employeeBySalaryMap.put(e.getAge(), list);
            }
        }

        Map<Integer, Double> salaryAverageMap = new HashMap<>();
        for (Map.Entry<Integer, List<Integer>> entry : employeeBySalaryMap.entrySet()) {
            List<Integer> salaries = entry.getValue();
            int sum = 0;

            for (int salary : salaries) {
                sum += salary;
            }

            salaryAverageMap.put(entry.getKey(), (double) sum / salaries.size());
        }

        return salaryAverageMap;

    }

}
