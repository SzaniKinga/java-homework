<%-- 
    Document   : car
    Created on : Jun 16, 2020, 5:39:21 PM
    Author     : kopacsi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Booking</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        <form action="BookingServlet" method="post">
            <label for="carID">Car ID:</label><br>
            <input type="text" id="carID" name="carID"><br><br>
            <label for="startDate">Start Date</label><br>
            <input type="date" id="startDate" name="startDate"><br><br>
            <label for="endDate">End Date</label><br>
            <input type="date" id="endDate" name="endDate"><br><br>
            <input type="submit" value="Submit">
        </form> 
    </body>
</html>
