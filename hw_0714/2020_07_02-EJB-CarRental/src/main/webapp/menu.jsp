<%-- 
    Document   : menu
    Created on : Jun 16, 2020, 6:57:34 PM
    Author     : kopacsi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main menu</title>
    </head>
    <body>
        <a href="CarServlet" >Car Servlet</a><br>
        <a href="SiteServlet" >Site Servlet</a><br>
        <a href="BookingServlet" >Booking Servlet</a>
    </body>
</html>
