package braininghub.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

@Stateless
public class CarDAO {
    
    @Resource(name = "jdbc/bh")
    DataSource ds;
    
    public void addCar(int siteID, String details) {
        String insertQuery = "insert into car(site_id, details) values(?, ?)";
        try (Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement(insertQuery);) {
            st.setInt(1, siteID);
            st.setString(2, details);
            conn.close();
            
            int rowsCreated = st.executeUpdate();
            
            System.out.println(rowsCreated + " cars created.");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
