package braininghub.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

@Stateless
public class SiteDAO {
    
    @Resource(name = "jdbc/bh")
    DataSource ds;
    
    public void addSite(String address) {
        String insertQuery = "insert into site (address) values(?)";
        try (Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement(insertQuery);) {
            st.setString(1, address);
            conn.close();
            
            int rowsCreated = st.executeUpdate();
            
            System.out.println(rowsCreated + " sites created.");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
