package braininghub.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

@Stateless
public class BookingDAO {
    
    @Resource(name = "jdbc/bh")
    DataSource ds;
    
    public void addBooking(int carID, LocalDate startDate, LocalDate endDate) {
        String insertQuery = "insert into booking (car_id, start_date, end_date) values(?, ?, ?)";
        try (Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement(insertQuery);) {
            st.setInt(1, carID);
            st.setDate(2, Date.valueOf(startDate));
            st.setDate(3, Date.valueOf(endDate));

            conn.close();
            
            int rowsCreated = st.executeUpdate();
            
            System.out.println(rowsCreated + " bookings created.");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
