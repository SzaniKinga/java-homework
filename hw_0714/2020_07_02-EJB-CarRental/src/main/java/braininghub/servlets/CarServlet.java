package braininghub.servlets;

import braininghub.dao.CarDAO;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CarServlet")
public class CarServlet extends HttpServlet {

    @EJB
    CarDAO carDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("car.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Integer siteID = Integer.valueOf(req.getParameter("siteID"));
        String details = req.getParameter("details");

        carDAO.addCar(siteID, details);
        
        res.sendRedirect("MainMenuServlet");
    }
}
