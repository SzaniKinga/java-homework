
package hw0407_01;

public class Banana extends Fruit {

    public Banana(String colour, double pricePerKg) {
        super(colour, pricePerKg);
    }

    @Override
    public double getPricePerKg() {
        return super.getPricePerKg()/100.00;
    }

    @Override
    public void setPricePerKg(int pricePerKg) {
        super.setPricePerKg(pricePerKg); 
    }

    @Override
    public String toString() {
        return "Banana: colour: " + colour + ", price per dkg: " + getPricePerKg();
    }

}
