/*Legyen egy Fruit osztalyod. Minden Fruitnak van valamilyen szine es 
kilogrammonkenti ara. Ebbol szarmaztass le egy Banana, Apple, Orange osztalyt. 
A Banana osztaly specialis, mert annak mindig a dekagrammonkenti arat szeretnenk 
megtudni. Az Applenek van iz tipusa is. Pl lehet fanyar, edes, stb.. egy Apple. 
Az Orange mindig naranccsarga szinu.*/
package hw0407_01;

public class Main {

    public static void main(String[] args) {
        Fruit fruit1 = new Fruit ("blue", 586);
        System.out.println(fruit1);
        Fruit banana1 = new Banana ("yellow", 670);
        System.out.println(banana1);
        Fruit apple1 = new Apple("sweet", "green", 891);
        System.out.println(apple1);
        Fruit orange1 = new Orange("yellow", 920);
        System.out.println(orange1);
    }
    
}
