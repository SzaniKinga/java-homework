
package hw0407_01;

public class Orange extends Fruit {

    public Orange(String colour, double pricePerKg) {
        super(colour, pricePerKg);
        super.colour = "orange";
    }
 
    @Override
    public void setColour(String colour) {
        super.setColour(colour);
    }

    @Override
    public String getColour() {
        return super.getColour();
    }

    @Override
    public String toString() {
        return "Orange: colour: " + getColour() + ", price per kg: " + getPricePerKg();
    }

}
