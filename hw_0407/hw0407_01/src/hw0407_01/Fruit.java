/*Legyen egy Fruit osztalyod. Minden Fruitnak van valamilyen szine es 
kilogrammonkenti ara. Ebbol szarmaztass le egy Banana, Apple, Orange osztalyt. 
A Banana osztaly specialis, mert annak mindig a dekagrammonkenti arat szeretnenk 
megtudni. Az Applenek van iz tipusa is. Pl lehet fanyar, edes, stb.. egy Apple. 
Az Orange mindig naranccsarga szinu.*/
package hw0407_01;

public class Fruit {
    protected String colour;
    private double pricePerKg;

    public Fruit(String colour, double pricePerKg) {
        this.colour = colour;
        this.pricePerKg = pricePerKg;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public double getPricePerKg() {
        return pricePerKg;
    }

    public void setPricePerKg(int pricePerKg) {
        this.pricePerKg = pricePerKg;
    }

    @Override
    public String toString() {
        return "Fruit: colour: " + colour + ", price per kg: " + pricePerKg;
    }

}
