
package hw0407_01;

public class Apple extends Fruit {
    private String flavour;

    public Apple(String flavour, String colour, double pricePerKg) {
        super(colour, pricePerKg);
        this.flavour = flavour;
    }

    @Override
    public void setPricePerKg(int pricePerKg) {
        super.setPricePerKg(pricePerKg);
    }

    @Override
    public double getPricePerKg() {
        return super.getPricePerKg(); 
    }
    
    @Override
    public String toString() {
        return "Apple: flavour: " + flavour + ", colour: " + colour + ", price per kg: " + getPricePerKg();
    }
    
}
