CREATE DATABASE IF NOT EXISTS contactlibrary;

USE contactlibrary;

DROP TABLE IF EXISTS messages;

CREATE TABLE IF NOT EXISTS messages (
	id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(127),
	subject VARCHAR(127),
    message VARCHAR(265)
);


