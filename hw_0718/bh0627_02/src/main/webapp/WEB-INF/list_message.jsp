<%-- 
    Document   : list
    Created on : 2020.06.27., 15:56:08
    Author     : Kinga
--%>

<%@page import="hu.braininghub.bh0627_02.model.MessageDTO"%>
<%@page import="hu.braininghub.bh0627_02.model.Message"%>
<%@page import="java.util.List"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Referenciák</title>

        <%@include file="head.jsp" %>
    </head>
    <body>
        <div class="container">
            <%@include file="menu.jsp" %>
            <% Message message = (Message) request.getAttribute("message");%>

            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th>Message</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${messageList}" var="message">
                            <tr>
                                <td>${message.id}</td>
                                <td>${message.email}</td>
                                <td>${message.subject}</td>
                                <td>${message.message}</td>
                                <td><a class="button" href="editMessage?messageId=${message.id}">editMessage</a></td>
                                <td><a class="button" href="removeMessage?messageId=${message.id}">removeMessage</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
    </body>
</html>

