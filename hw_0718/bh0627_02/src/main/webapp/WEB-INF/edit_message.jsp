<%@page import="hu.braininghub.bh0627_02.model.Message"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit message</title>
    </head>
    <body>
        <% Message message = (Message) request.getAttribute("message");%>
        <h1>Edit message</h1>
        
        <form action="editMessage?messageId=${message.id}" method="post">
            <label for="message">Message text</label><br>
            <textarea id="message" name="message">${message.message}</textarea>
            <br><br>
            <input type="submit" value="Submit">
        </form> 
    </body>
</html>
