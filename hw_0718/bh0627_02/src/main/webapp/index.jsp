<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bemutatkozás</title>
        <%@include file="WEB-INF/head.jsp" %>
    </head>
    <body>
        <div class="container">
            <%@include file="WEB-INF/menu.jsp" %>
            <div class="row">
                <div class="col-2">
                    <img src="https://i0.wp.com/holdkomp.hu/wp-content/uploads/2017/11/john-doe.jpg" width="180" class="rounded" alt="" title=""/>
                    <p>
                        John Doe <br />
                        Javascript developer
                        <%= session.getAttribute("sentMessage")%>
                    </p>
                </div>
                <div class="col-10">
                    <h2>About me: </h2>
                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</p>
                    <h2>Tanulmányaim</h2>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Intézmény</th>
                                <th>időpont</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Élet iskolája</td>
                                <td>2008</td>
                            </tr>
                            <tr>
                                <td>Egyetem</td>
                                <td>2001-2008</td>
                            </tr>                        
                            <tr>
                                <td>Középsuli</td>
                                <td>1997-2001</td>
                            </tr>                                
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
