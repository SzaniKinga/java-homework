<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Referenciák</title>

        <%@include file="WEB-INF/head.jsp" %>    
    </head>
    <body>
        <div class="container">
            <%@include file="WEB-INF/menu.jsp" %>

            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Cégnév</th>
                            <th>Beosztás</th>
                            <th>időpont</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Pékség</td>
                            <td>Pék</td>
                            <td>2008-napjainkig</td>
                        </tr>
                        <tr>
                            <td>Kocsma</td>
                            <td>tesztelő</td>
                            <td>2001-napjainkig</td>
                        </tr>                    
                    </tbody>
                </table>
            </div>
    </body>
</html>
