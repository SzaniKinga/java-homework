<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Kapcsolat</title>
        
        <%@include file="WEB-INF/head.jsp" %>
        <script>
            let counter = 0;
            
            function validateForm() {
                let subjectElement = document.getElementById("subject");
                let messageElement = document.getElementById("message");
                if(subjectElement.value.length < 10) {
                    document.getElementById("errorBox").style.display = 'block';
                    return false;
                }              
                return true;
            }
            
            function emailChange() {
                console.log("email changed");
            }
            function emailKeyPressed() {
                console.log(counter++);
            }
        </script>
    </head>
    <body>
        <div class="container">
            <%@include file="WEB-INF/menu.jsp" %>
            <div class="alert alert-danger" id="errorBox" style="display: none;">
              <strong>Nem valid a form!</strong>
            </div>
            <form method="POST" action="sendMessage" onsubmit="return validateForm()">
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" placeholder="Enter email" id="email" name="email" onkeypress="emailKeyPressed()" onchange="emailChange()">
                </div>
                <div class="form-group">
                    <label for="subject">Subject:</label>
                    <input type="text" class="form-control" placeholder="Enter subject" id="subject" name="subject">
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea id="message" name="message" class="form-control"> 
                    </textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </body>
</html>
