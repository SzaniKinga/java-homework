/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh0627_02.servlet;

import hu.braininghub.bh0627_02.model.Message;
import hu.braininghub.bh0627_02.service.MessageService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "EditMessageServlet", urlPatterns = {"/editMessage"})
public class EditMessageServlet extends HttpServlet {

    @Inject
    private MessageService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        final String id = request.getParameter("messageId");
        if (id != null) {
            int messageId = Integer.parseInt(request.getParameter("messageId"));
            request.setAttribute("message", service.getMessageById(messageId));

            request.getRequestDispatcher("WEB-INF/edit_message.jsp").forward(request, response);
        } 
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final String id = request.getParameter("messageId");
        if (id != null) {
            int messageId = Integer.parseInt(request.getParameter("messageId"));
            String messageText = request.getParameter("message");
            service.editMessage(messageId, messageText);
            
            response.sendRedirect(request.getContextPath() + "/ListMessage");
        }
    }
}
