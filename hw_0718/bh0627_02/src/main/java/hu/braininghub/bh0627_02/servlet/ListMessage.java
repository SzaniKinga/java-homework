/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh0627_02.servlet;

import hu.braininghub.bh0627_02.service.MessageService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ListMessage", urlPatterns = {"/ListMessage"})
public class ListMessage extends HttpServlet {

    //private MessageService service = new MessageService(); -> ez már nem kell
    @Inject //injektáljuk a stateless beant (messageservice) -> a bean életciklusát a container fogja vezetni, nem kell nekünk példányosítani!!!
    private MessageService service; //messageservice-t majd a container példányosítja
    //dependency injection: interface-ektől függünk, nem konkrét implementációktól, így nem kell kódot változtatni

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            request.setAttribute("messageList", service.getMessages());
        } catch (SQLException ex) {
            Logger.getLogger(ListMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("message", service.getMessageById(1));
        request.getRequestDispatcher("WEB-INF/list_message.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
