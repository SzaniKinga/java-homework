/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh0627_02.service;

import hu.braininghub.bh0627_02.model.Message;
import hu.braininghub.bh0627_02.model.MessageDAO;
import hu.braininghub.bh0627_02.model.MessageDTO;
import java.sql.SQLException;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless //beant csinálunk belőle
public class MessageService {

    //MessageDAO dao = new MessageDAO(); -> ez már nem kell
    @Inject //ez fogja példányosítani a beant
    MessageDAO dao; //ezt is injektáljuk, mert már bean lett

    /*public void addMessage(String email, String subject, String message) {
        dao.addNewMessage(email, subject, message);
    }*/
    
    public void addMessage(String email, String subject, String message) {
        MessageDTO dto = new MessageDTO();
        dto.setEmail(email);
        dto.setSubject(subject);
        dto.setMessage(message);
        
        dao.addNewMessage(dto);
    }

    public void removeMessage(int messageId) {
        dao.removeMessage(messageId);
    }

    public List<MessageDTO> getMessages() throws SQLException {
        return dao.getMessages();
    }

    public Message getMessageById(int id) {
        return dao.getMessageById(id);
    }
    
    public void editMessage(int id, String text) {
        Message m = getMessageById(id);
        m.setMessage(text);
        dao.editMessage(m);
    }

}
