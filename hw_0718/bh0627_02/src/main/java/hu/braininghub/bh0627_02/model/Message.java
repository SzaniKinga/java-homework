/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh0627_02.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //entitás bean lesz ez az oszály
@Table(name = "messages") //Message objektumot meg akarom feleltetni az adatbázisban lévő messages táblának
public class Message implements Serializable {

    @Id //kell neki id, és hogy felismerje
    @Column(name="id") //erre az "id" nevű adatbázisváltozóra vonatkozzon az id annotáció
    private int id;
    
    private String email;
    private String subject;
    private String message;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
