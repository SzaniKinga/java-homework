/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh0627_02.model;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton //bean lesz
public class MessageDAO {

    @PersistenceContext
    private EntityManager em;

    /*public void addNewMessage(String email, String subject, String message) {
        Message m = new Message();
        m.setEmail(email);
        m.setSubject(subject);
        m.setMessage(message);
        em.persist(m); //egy elem eltárolása
    }*/
    
    public void addNewMessage(MessageDTO dto) {
        Message m = MessageMapper.toEntity(dto);
        em.persist(m); //egy elem eltárolása
    }
    
    public void removeMessage(int messageId) {
        Message m = this.getMessageById(messageId);
        em.remove(m);
    }

    public Message getMessageById(int id) {
        Message m = em.find(Message.class, id); //egy elem megtalálása
        return m;
    }

    public List<MessageDTO> getMessages() { //a listát akarom lekérni adatbázisból
        List<Message> messages = em.createQuery("SELECT m FROM Message m").getResultList(); //ez a JPQL, utolsó m egy alias név, getResultList olyan mint az executeQuery
        List<MessageDTO> messageDTOs = new ArrayList();
        
        for(Message message: messages){
            messageDTOs.add(MessageMapper.toDTO(message)); //itt átmappeljük DTO-ra
        }
        
        return messageDTOs;
    }
    
    public void editMessage(Message m) {
        em.merge(m);
    }

}
