/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh0627_02.servlet;

import hu.braininghub.bh0627_02.service.MessageService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "MessageServlet", urlPatterns = {"/sendMessage"})
public class MessageServlet extends HttpServlet {

    //private MessageService service = new MessageService(); -> ez már nem kell
    @Inject
    private MessageService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("contact.jsp").forward(req, res);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        HttpSession session = req.getSession(); //ezt mindig a servletben hozom létre, a requesthez kapcsolódik
        session.setAttribute("sentMessage", "üzenetet elküldtük"); //sendMessage TRUE el lesz tárolva, amíg a böngészőt be nem zárja vagy meg nem szüntetem a sessiont
        //ha ezt el akarom érni, akkor jsp-ben getAttribute-ot kell használni

        //session.setAttribute("messages", service.getMessages());  ebben lista lesz
        String email = req.getParameter("email");
        String subject = req.getParameter("subject");
        String message = req.getParameter("message");
        service.addMessage(email, subject, message);

        res.sendRedirect(req.getContextPath() + "/index.jsp");  //csinál egy új http kérést, végén már index.jsp lesz az url-ben, ha itt refreshelek ctrl r, akkor az index jsp-n maradok
        //postnál sendRedirect kell!!!
        //req.getRequestDispatcher("index.jsp").forward(req, res);  //ide nem kell a getContextPath, egy http kérésen belül maradunk, ez egy másik servletre irányítja tovább a kérést
        //ez itt nem jó, mert újraküldi refreshnél a bemenő adatokat is...
    }

}
