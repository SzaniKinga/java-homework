/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh0627_02.model;

public class MessageMapper { //vmilyen típusról másik típusra váltunk: Message (Entity) -> MessageDTO
    //ez azért kell, mert Entity-t nem adunk ki a DAO-ból
    //view rétegbe már DTO megy!! (Servletbe, jsp-be)

    public static MessageDTO toDTO(Message message) {
        MessageDTO dto = new MessageDTO();
        
        dto.setEmail(message.getEmail());
        dto.setSubject(message.getSubject());
        dto.setMessage(message.getMessage());
        dto.setId(message.getId());

        return dto;
    }
    
    public static Message toEntity(MessageDTO dto) {
        Message message = new Message();
        
        message.setEmail(dto.getEmail());
        message.setSubject(dto.getSubject());
        message.setMessage(dto.getMessage());
        message.setId(dto.getId());

        return message;
    }
    
    //toEntity method DTO -> Entity

}
