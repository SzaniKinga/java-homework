/*Írjuk ki egy fájl tulajdonságait (fájl-e?, útvonal, abszolút útvonal, szülőmappa, 
fájl neve, mérete, olvasható-e, írható-e, rejtett-e, utolsó módosítás dátuma)*/
package bh0425_18;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class Bh0425_18 {

    public static void main(String[] args) {
        File f = new File("test.txt");

        readAndPrintFile(f);

        System.out.println("");
        System.out.println("Is file " + f.isFile());
        System.out.println("Path " + f.getPath());
        System.out.println("Abs path " + f.getAbsolutePath());
        System.out.println("Get parent file " + f.getParentFile());
        System.out.println("Get name " + f.getName());
        System.out.println("Total space " + f.getTotalSpace());
        System.out.println("Can read " + f.canRead());
        System.out.println("Can write " + f.canWrite());
        System.out.println("Is hidden " + f.isHidden());
        System.out.println("Last modified " + f.lastModified());
        Date date = new Date(f.lastModified());
        System.out.println("Last modified " + date);

    }

    public static void readAndPrintFile(File f) {
        try {
            FileWriter writer = new FileWriter(f);
            writer.write("Hello world");
            writer.close();
        } catch (IOException e) {
            System.out.println("A fájl nem található!");
        }

        try (FileReader reader = new FileReader(f)) {

            int charInt = reader.read();
            do {
                char c = (char) charInt;
                System.out.print(c);
                charInt = reader.read();

            } while (charInt != -1);
            reader.close();

        } catch (IOException e) {
            System.out.println("A fájl nem található!");
        }
    }

}
