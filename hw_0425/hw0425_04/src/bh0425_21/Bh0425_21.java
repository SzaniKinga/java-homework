/*A program olvassa be a „scanner_text.txt” fájl tartalmát és írja ki a konzolra*/
package bh0425_21;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class Bh0425_21 {

    public static void main(String[] args) {

        File f = new File("scanner_text.txt");

        char[] charBuffer = new char[10];

        try (FileReader fr = new FileReader(f)) {
            int readCount = fr.read(charBuffer);
            while (readCount != -1) {
                System.out.print(new String(charBuffer, 0, readCount)); 
                readCount = fr.read(charBuffer);  
            }

        } catch (IOException e) {
            System.out.println("Hibás fájl");
        }

    }
}
