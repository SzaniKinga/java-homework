/*Írjuk ki a felhasználó által megadott útvonalban lévő összes fájl illetve mappa nevet.*/
package bh0425_19;

import java.io.File;
import java.util.Scanner;

public class Bh0425_19 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String path = sc.nextLine();

        File f = new File(path);

        for (String s : f.list()) {
            System.out.println(s);
        }
    }
}
