/*Kérjünk be a felhasználótól szöveges sorokat addig, amíg egy üres sort nem ír be.
A sorokat sorszámmal írjuk ki egy fájlba „scanner_text.txt” néven.*/
package bh0425_20;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class Bh0425_20 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        File f = new File("scanner_text.txt");
        
        int lineCounter = 1;
        try (FileWriter fw = new FileWriter(f)) {
            String line;
            do {
                line = sc.nextLine();
                fw.write(lineCounter + ". " + line + "\n");
                lineCounter++;
            } while (!line.equals(""));

        } catch (IOException e) {
            System.out.println("Fájlhiba történt");
        }

    }

}
