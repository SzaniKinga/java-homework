/*8. Az előző feladatokat módosítsuk úgy, hogy a program minden egyes futásnál az épp 
aktuális idő szerinti mappába teszi a szelvényeket, illetve az összes ilyen mappát 
olvassuk fel és írjuk ki a konzolra.*/
package hw0425_08;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Hw0425_08 {

    public static int[] lotteryArray = new int[5];

    public static void main(String[] args) {
        long millis = System.currentTimeMillis();
        java.util.Date date = new java.util.Date(millis);
        //System.out.println(date);
        String dir = "lottery/" + date.getHours() + "." + date.getMinutes() + "." + date.getSeconds();
        File d = new File(dir);
        d.mkdirs();
        File f = new File(dir + "/lottery.txt");

        Scanner sc = new Scanner(System.in);
        System.out.println("Kérem, adja meg, hány db lottószelvényt kér");
        int number = sc.nextInt();
        String lotteryText = "";

        try {
            FileWriter writer = new FileWriter(f);

            for (int j = 0; j < number; j++) {
                lotteryText = "";
                for (int i = 0; i < 5; i++) {
                    lotteryArray[i] = generateRandomLotteryNumber(i);
                }
                for (int i = 0; i < 5; i++) {
                    lotteryText += lotteryArray[i] + " ";
                }
                System.out.println(lotteryText);
                writer.write(lotteryText + "\n");
            }
            writer.close();
        } catch (IOException ex) {
            System.out.println("A fájl nem található!");
        }

        System.out.println("--------------");

        File[] files = new File("lottery").listFiles();

        showFiles(files);
    }

    public static void showFiles(File[] files) {
        for (File file : files) {
            if (file.isDirectory()) {
                System.out.println(file.getName() + ":");
                showFiles(file.listFiles());
            } else {
                try (FileReader reader = new FileReader(file)) {

                    int charInt = reader.read();
                    do {
                        char c = (char) charInt;
                        System.out.print(c);
                        charInt = reader.read();

                    } while (charInt != -1);
                    reader.close();
                } catch (IOException e) {
                    System.out.println("A fájl nem található!");
                }
            }
        }
    }

    public static int generateRandomLotteryNumber(int index) {
        boolean unique;
        int number;
        do {
            unique = true;
            number = (int) (Math.random() * (90) + 1);
            for (int i = 0; i < index; i++) {
                if (lotteryArray[i] == number) {
                    unique = false;
                    break;
                }
            }
        } while (!unique);
        return number;
    }

}
