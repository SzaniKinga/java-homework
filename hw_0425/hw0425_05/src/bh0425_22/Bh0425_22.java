/*A program kérjen be egy elérési és egy cél útvonalat. Az elérési útvonalon lévő 
fájlt tartalmával együtt másolja át a cél útvonalra. Ha a megadott fájl egy mappa, 
akkor dobjunk kivételt amit le is kezelünk. Ha bármi hiba történik (például a cél 
útvonal ugyanaz mint az elérési útvonal) akkor is dobjunk kivételt, és kezeljük le.*/
package bh0425_22;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Bh0425_22 {

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);

        String sourcePath = sc.nextLine();
        String targetPath = sc.nextLine();

        File sourceFile = new File(sourcePath);
        File targetFile = new File(targetPath);

        try (BufferedReader br = new BufferedReader(new FileReader(sourceFile));
                FileWriter fw = new FileWriter(targetPath)) {
            String line = br.readLine();
            while (line != null) {
                System.out.println(line);
                line = br.readLine();
            }

        } catch (IOException e) {
        }

    }
}
