/*6. Készítsen lottó szelvény készítő programot. A program a felhasználó által megadott 
darabszámú lottó szelvényt generáljon, és mindet egy külön fájlba írja ki olyan 
formátumban, hogy majd ezt később be is tudjuk olvasni.
7. Olvassuk be az előző feladatban generált összes lottószelvényt, és írjuk ki őket 
soronként a konzolra.*/
package hw0425_06;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Hw0425_06 {

    public static int[] lotteryArray = new int[5];

    public static void main(String[] args) {
        File f = new File("test.txt");

        Scanner sc = new Scanner(System.in);
        System.out.println("Kérem, adja meg, hány db lottószelvényt kér");
        int number = sc.nextInt();
        String lotteryText = "";

        try {
            FileWriter writer = new FileWriter(f);

            for (int j = 0; j < number; j++) {
                lotteryText = "";
                for (int i = 0; i < 5; i++) {
                    lotteryArray[i] = generateRandomLotteryNumber(i);
                }
                for (int i = 0; i < 5; i++) {
                    lotteryText += lotteryArray[i] + " ";
                }
                System.out.println(lotteryText);
                writer.write(lotteryText + "\n");
            }
            writer.close();
        } catch (IOException ex) {
            System.out.println("A fájl nem található!");
        }

        System.out.println("--------------");

        try (FileReader reader = new FileReader(f)) {

            int charInt = reader.read();
            do {
                char c = (char) charInt;
                System.out.print(c);
                charInt = reader.read();

            } while (charInt != -1);
            reader.close();
        } catch (IOException e) {
            System.out.println("A fájl nem található!");
        }

    }

    public static int generateRandomLotteryNumber(int index) {
        boolean unique;
        int number;
        do {
            unique = true;
            number = (int) (Math.random() * (90) + 1);
            for (int i = 0; i < index; i++) {
                if (lotteryArray[i] == number) {
                    unique = false;
                    break;
                }
            }
        } while (!unique);
        return number;
    }

}
