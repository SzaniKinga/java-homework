/*Csináljunk egy mini játékot. Hozzunk létre 9 darab gombot, használjuk a Grid 
Layout-ot. Egy gomb megnyomására, tűnjön el egy random másik gomb, viszont az 
eddig eltűntetett gomb jelenjen meg!*/
package hw0521_01;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class MainFrame extends JFrame implements ActionListener {

    private JButton[] button = new JButton[9];

    public MainFrame(String title) {
        super(title);
        int num = 1;
        for (int i = 0; i < button.length; i++) {
            button[i] = new JButton(Integer.toString(num));
            num++;
        }
    }

    public void init() {
        setSize(400, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        for (int i = 0; i < button.length; i++) {
            this.add(button[i]);
        }

        setLayout(new GridLayout(3, 3));

        for (int i = 0; i < button.length; i++) {
            button[i].addActionListener(this);
        }

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int index = (int) (Math.random() * 9);
        for (int i = 0; i < button.length; i++) {
            if (!button[i].isVisible()) {
                button[i].setVisible(true);
            }
            if (i != index) {
                button[index].setVisible(false);
            }
        }

    }

}
