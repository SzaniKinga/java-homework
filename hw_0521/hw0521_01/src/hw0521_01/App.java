/*Csináljunk egy mini játékot. Hozzunk létre 9 darab gombot, használjuk a Grid 
Layout-ot. Egy gomb megnyomására, tűnjön el egy random másik gomb, viszont az 
eddig eltűntetett gomb jelenjen meg!*/
package hw0521_01;

public class App {

    public static void main(String[] args) {
        MainFrame frame = new MainFrame("Swing");
        frame.init();
    }
    
}
